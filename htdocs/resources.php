<?php readfile("header.php"); ?>


<div style='border:0;width:80%'>
<h2>External resources on conserved non-coding elements:</h2>

<ul>
<li><a href='http://condor.nimr.mrc.ac.uk/' target='_blank'>CONDOR</a> - database resource of developmentally-associated conserved non-coding elements. </li>
<br>
<li><a href='http://enhancer.lbl.gov/frnt_page_n.shtml' target='_blank'>VISTA Enhancer Browser</a> — database of tissue-specific human enhancers. </li>
<br>
<li><a href='http://ancora.genereg.net/' target='_blank'>Ancora</a> - web resource for exploring highly conserved noncoding elements and their association with developmental regulatory genes. </li>
<br>
<li><a href='http://corg.eb.tuebingen.mpg.de/cgi-bin/index.pl' target='_blank'>CORG</a> - database for COmparative Regulatory Genomics. </li>
<br>
<li><a href='http://bioinformatics.bc.edu/chuanglab/cneViewer/' target='_blank'>cneViewer</a> - database of conserved non-coding elements for studies of tissue-specific gene regulation. </li>
<br>
</ul>
<h2>Other resources and tools by the <a href='https://www.sib.swiss/philipp-bucher-group'>Computational Cancer Genomics (CCG)</a> group of the <a href='https://www.sib.swiss/'>SIB Swiss Institute of Bioinformatics</a>:</h2>
<ul>
<li><a href='/epd/' target='_blank'>EPD</a> - the Eukaryotic Promoter Database. </li>
<br>
<li><a href='/chipseq/' target='_blank'>ChIP-Seq</a> - ChIP-Seq Analysis server. </li>
<br>
<li><a href='/ssa/' target='_blank'>SSA</a>  - Signal Search Analysis server. </li>
<br>
<li><a href='/madap/' target='_blank'>MADAP</a> - clustering tool for the interpretation of one-dimensional genome annotation data </li>
<br>
<li><a href='/tagger/tagscan.html' target='_blank'>TagScan</a> - fast sequence tag mapping. </li>
<br>
<li><a href='/tromer/' target='_blank'>TROMER Database</a> - transcriptome analyser documenting all transcribed elements. </li>
<br>
<li><a href='http://cleanex.vital-it.ch/' target='_blank'>CleanEx</a> - database that provides access to public gene expression data via unique approved gene symbols. </li>
<br>
<li><a href='https://myhits.sib.swiss/' target='_blank'>myHITS</a> - database and web tools devoted to protein domains. </li>
<br>
</ul>
</div>


<br><br>


<!-- ######### Insert the footer #########-->
<?php readfile("footer.html"); ?>

</body>
</html>
