<?php readfile("header.php"); ?>


<div style='font-size: 12px; text-align: justify; width:95%;'>
<br/><br/>
<strong>UCNEbase</strong> provides information on the evolution and genomic organization of <strong>ultra-conserved non-coding elements (UCNEs)</strong>
in multiple vertebrate species. It currently covers 4351 such elements in 18 different species. Around half of these elements
are located within intergenic regions (2'139) and the rest are located within non-coding parts of genes: introns (1'713) and UTRs (499).
The majority of UCNEs are supposed to be transcriptional regulators of key developmental genes. <br/><br/>
As most UCNEs occur as clusters near key developmental genes, our resource is organized
along two hierarchical levels: <ul><li>individual UCNEs and</li> <li>genomic regulatory blocks (GRBs).</li></ul>
UCNEbase introduces a coherent nomenclature for UCNEs reflecting their respecting associations with likely target genes.
Orthologous and paralogous UCNEs share components of their names and are systematically cross-linked. Detailed synteny maps between the human and other genomes are provided for all UGRBs.
<br/><br/>UCNEbase is potentially useful to
any computational or experimental biologist interested in the evolution and function of <em>cis</em>-regulatory elements near
<em>trans-dev</em> genes, and in particular to those interested in the syntenic conservation of GRBs and in cooperative
interactions between UCNEs of the same block.<br/><br/>
Details on the database can be found using the <a class='link' href="./documentation.php">Documentation page</a> or in our publication:<br/>
<ul><li><a href="https://dx.doi.org/10.1093%2Fnar%2Fgks1092">UCNEbase - a database of ultra-conserved non-coding elements and genomic regulatory blocks.</a><br>Dimitrieva, S. and Bucher, P. <em>Nucleic Acids Research</em>, Volume 41, Pages D101-D109 (2013)</li></ul>

</div>


<!-- ######### Insert the footer #########-->
<?php readfile("footer.html"); ?>

</body>
</html>
