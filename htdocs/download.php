<?php readfile("header.php"); ?>


<div style='font-size: 12px; text-align: justify;'>
<p>
	<hr/><h2 >Download individual UCNEs</h2> <hr/>
	<h3 style='margin-left:1cm;'>Genomic coordinates of identified UCNEs (BED format)</h3>
  		<p style='margin-left:2cm; line-height: 2.0;'>
  		<a class='link' href='./data/download/ucnes/hg19_UCNE_coord.bed'><img src='./Icons/dload.png'> Human UCNEs (hg19 assembly)</a><br/>
  		<a class='link' href='./data/download/ucnes/galGal3_UCNE_coord.bed'><img src='./Icons/dload.png'> Chicken UCNEs (galGal3 assembly)</a></p>
		<small style='margin-left:2cm;'> Note: The 4th column corresponds to the given UCNE name; the 5th column corresponds to an internal ID of the UCNE. </small>
	<h3 style='margin-left:1cm;'>UCNE homologs in species</h3>
  		<p style='margin-left:2cm; line-height: 2.0;'>
  		<a class='link' href='./data/download/ucnes/mm10_UCNE_orthologs.txt'><img src='./Icons/dload.png'> Mouse (mm10)</a>
  		<a class='link' href='./data/download/ucnes/dasNov1_UCNE_orthologs.txt'><img src='./Icons/dload.png'> Armadillo (dasNov1)</a>
  		<a class='link' href='./data/download/ucnes/monDom5_UCNE_orthologs.txt'><img src='./Icons/dload.png'> Opossum (monDom5)</a>
  		<a class='link' href='./data/download/ucnes/ornAna1_UCNE_orthologs.txt'><img src='./Icons/dload.png'> Platypus (ornAna1)</a> <br/>
  		<a class='link' href='./data/download/ucnes/taeGut1_UCNE_orthologs.txt'><img src='./Icons/dload.png'> Zebra finch (taeGut1)</a>
  		<a class='link' href='./data/download/ucnes/anoCar2_UCNE_orthologs.txt'><img src='./Icons/dload.png'> Lizard (anoCar2)</a>
  		<a class='link' href='./data/download/ucnes/chrPic1_UCNE_orthologs.txt'><img src='./Icons/dload.png'> Painted turtle (chrPic1)</a>
  		<a class='link' href='./data/download/ucnes/xenTro3_UCNE_orthologs.txt'><img src='./Icons/dload.png'> Xenopus (xenTro3)</a> <br/>
  		<a class='link' href='./data/download/ucnes/fr2_UCNE_orthologs.txt'><img src='./Icons/dload.png'> Fugu (fr2)</a>
  		<a class='link' href='./data/download/ucnes/oryLat2_UCNE_orthologs.txt'><img src='./Icons/dload.png'> Medaka (oryLat2)</a>
  		<a class='link' href='./data/download/ucnes/gasAcu1_UCNE_orthologs.txt'><img src='./Icons/dload.png'> Stickleback (gasAcu1)</a>
  		<a class='link' href='./data/download/ucnes/tetNig2_UCNE_orthologs.txt'><img src='./Icons/dload.png'> Tetraodon (tetNig2)</a>
  		<a class='link' href='./data/download/ucnes/danRer7_UCNE_orthologs.txt'><img src='./Icons/dload.png'> Zebrafish (danRer7)</a> <br/>
   	<h3 style='margin-left:1cm;'>FASTA sequences of identified UCNEs</h3>
  		<p style='margin-left:2cm; line-height: 2.0;'>
  		<a class='link' href='./data/download/fasta/hg19_UCNEs.fasta.gz'><img src='./Icons/dload.png'> Human UCNEs fasta (hg19 assembly)</a><br/>
  		<a class='link' href='./data/download/fasta/galGal3_UCNEs.fasta.gz'><img src='./Icons/dload.png'> Chicken UCNEs fasta (galGal3 assembly)</a></p>
  	<h3 style='margin-left:1cm;'>Paralogs of identified UCNEs</h3>
  		<p style='margin-left:2cm; line-height: 2.0;'>
  		<a class='link' href='./data/download/ucnes/UCNE_paralog_regions.txt'><img src='./Icons/dload.png'> Human UCNE paralogs (hg19 assembly)</a></p>
</p>

<p>
	<hr/><p style='background-color: #d7dfe2;'><h2>Download UCNE clusters</h2></p><hr/>
	<h3 style='margin-left:1cm;'>UCNE cluster names and gene association</h3>
  		<p style='margin-left:2cm; line-height: 2.0;'><a class='link' href='./data/download/clusters/cluster_names.txt'><img src='./Icons/dload.png'> UCNE clusters </a><br/>
	<h3 style='margin-left:1cm;'>Genomic coordinates of identified UCNE clusters (BED format)</h3>
  		<p style='margin-left:2cm; line-height: 2.0;'><a class='link' href='./data/download/clusters/hg19_clusters_coord.bed'><img src='./Icons/dload.png'> Human UCNEs (hg19 assembly)</a><br/>
  		<small> Note: The 4th column corresponds to the given cluster name; the 5th column corresponds to an internal ID of the UCNE cluster. </small>
	<h3 style='margin-left:1cm;'>Grouping of UCNEs into UCNE clusters</h3>
  		<p style='margin-left:2cm; line-height: 2.0;'><a class='link' href='./data/download/clusters/UCNEs_to_clusters.txt'><img src='./Icons/dload.png'> UCNEs per cluster</a><br/>
	<h3 style='margin-left:1cm;'>UCNE subclusters in species</h3>
  		<p style='margin-left:2cm; line-height: 2.0;'>
  		<a class='link' href='./data/download/clusters/mm10_subclusters.txt'><img src='./Icons/dload.png'> Mouse (mm10)</a>
  		<a class='link' href='./data/download/clusters/monDom5_subclusters.txt'><img src='./Icons/dload.png'> Opossum (monDom5)</a>
  		<a class='link' href='./data/download/clusters/ornAna1_subclusters.txt'><img src='./Icons/dload.png'> Platypus (ornAna1)</a>
  		<a class='link' href='./data/download/clusters/galGal3_subclusters.txt'><img src='./Icons/dload.png'> Chicken (galGal3)</a><br/>
  		<a class='link' href='./data/download/clusters/taeGut1_subclusters.txt'><img src='./Icons/dload.png'> Zebra finch (taeGut1)</a>
  		<a class='link' href='./data/download/clusters/anoCar2_subclusters.txt'><img src='./Icons/dload.png'> Lizard (anoCar2)</a>
  		<a class='link' href='./data/download/clusters/chrPic1_subclusters.txt'><img src='./Icons/dload.png'> Painted turtle (chrPic1)</a>
  		<a class='link' href='./data/download/clusters/xenTro3_subclusters.txt'><img src='./Icons/dload.png'> Xenopus (xenTro3)</a> <br/>
  		<a class='link' href='./data/download/clusters/fr2_subclusters.txt'><img src='./Icons/dload.png'> Fugu (fr2)</a>
  		<a class='link' href='./data/download/clusters/oryLat2_subclusters.txt'><img src='./Icons/dload.png'> Medaka (oryLat2)</a>
  		<a class='link' href='./data/download/clusters/gasAcu1_subclusters.txt'><img src='./Icons/dload.png'> Stickleback (gasAcu1)</a>
  		<a class='link' href='./data/download/clusters/tetNig2_subclusters.txt'><img src='./Icons/dload.png'> Tetraodon (tetNig2)</a>
  		<a class='link' href='./data/download/clusters/danRer7_subclusters.txt'><img src='./Icons/dload.png'> Zebrafish (danRer7)</a> <br/>
  	<hr/><p style='background-color: #d7dfe2;'><h2>Custom tracks for UCSC Genome Browser</h2></p><hr/>
  	<h3 style='margin-left:1cm;'>Human genome custom tracks</h3>
  		<p style='margin-left:2cm; line-height: 2.0;'>
  		All data from UCNEbase (organized in BED format) can be displayed as colored custom tracks using the UCSC Genome Browser. The regions are presented together with
  		their unique names and by clicking on a specific region users can be linked to the corresponding entry in UCNEbase.<br/>
  		The following file contains several tracks: track for all identified UCNEs; track for all UCNE clusters; track for the paralogous regions of the UCNEs;
  		track for all CONDOR CNEs; track for all VISTA elements and a track for all identified UCEs (Bejerano et al. 2004): <br/>
  		<p style='margin-left:5cm; line-height: 2.0;'><a class='link' href='./data/UCNEs_hg19.bed'><img src='./Icons/dload.png'> Human genome (hg19) custom tracks</a>
  	<h3 style='margin-left:1cm;'>Custom tracks for other vertebrates</h3>
  	<p style='margin-left:2cm; line-height: 2.0;'>
  		The following files contain several tracks: track for all identified UCNEs; track containing the UCNE sub-clusters in a species;
  		track containing the paralogous regions of the UCNEs in a species: <br/>
  		<a class='link' href='./data/UCNEs_mm10.bed'><img src='./Icons/dload.png'> Mouse (mm10)</a>
  		<a class='link' href='./data/UCNEs_monDom5.bed'><img src='./Icons/dload.png'> Opossum (monDom5)</a>
  		<a class='link' href='./data/UCNEs_ornAna1.bed'><img src='./Icons/dload.png'> Platypus (ornAna1)</a>
  		<a class='link' href='./data/UCNEs_gg3.bed'><img src='./Icons/dload.png'> Chicken (galGal3)</a><br/>
  		<a class='link' href='./data/UCNEs_taeGut1.bed'><img src='./Icons/dload.png'> Zebra finch (taeGut1)</a>
  		<a class='link' href='./data/UCNEs_anoCar2.bed'><img src='./Icons/dload.png'> Lizard (anoCar2)</a>
  		<a class='link' href='./data/UCNEs_chrPic1.bed'><img src='./Icons/dload.png'> Painted turtle (chrPic1)</a>
  		<a class='link' href='./data/UCNEs_xenTro3.bed'><img src='./Icons/dload.png'> Xenopus (xenTro3)</a> <br/>
  		<a class='link' href='./data/UCNEs_fr2.bed'><img src='./Icons/dload.png'> Fugu (fr2)</a>
  		<a class='link' href='./data/UCNEs_oryLat2.bed'><img src='./Icons/dload.png'> Medaka (oryLat2)</a>
  		<a class='link' href='./data/UCNEs_gasAcu1.bed'><img src='./Icons/dload.png'> Stickleback (gasAcu1)</a>
  		<a class='link' href='./data/UCNEs_tetNig2.bed'><img src='./Icons/dload.png'> Tetraodon (tetNig2)</a>
  		<a class='link' href='./data/UCNEs_danRer7.bed'><img src='./Icons/dload.png'> Zebrafish (danRer7)</a> <br/>
  	<h3 style='margin-left:1cm;'>Cluster specific tracks (one BED file per cluster) </h3>
  		<p style='margin-left:2cm; line-height: 2.0;'>
  		The following zipped file contains separate BED files for each identified UCNE cluster. Each file contains cluster specific tracks with all identified UCNEs within the cluster in <b>different vertebrate species mapped on the human genome</b>.
  		<br/><a class='link' href='./data/download/clusters/cluster_bed.zip'><img src='./Icons/dload.png'> Cluster specific tracks (one BED file per cluster)</a>

  	<hr/><p style='background-color: #d7dfe2;'><h2>Summary of UCNE subclusters in fishes (considering fish specific whole genome duplication)</h2></p><hr/>
  	<p style='margin-left:1cm; line-height: 2.0;'>
  		The excel files below show a summary of UCNE subclusters identified in fish genomes. <br/><b><i>Note:</i></b> The (+) or (-) sign after the gene name denotes the gene orientation. For the fish orthologous genes
		we present the Ensembl ID together with the human gene name. The (r) or (p) sign denotes whether the ortholog is a "real" or "possible"
		ortholog, according to the annotation in Ensembl v64. We performed manual curration on the identified subclusters only for the top 25 clusters ranked by the number of UCNEs.
		The manual curration field indicates whether the fragment has been assigned to the major or minor subcluster or it is
		unassigned duplicate. Unassigned duplicate denotes that the same UCNE is found in the major or minor subcluster and it is not used in the statistics.
  		<ul style='margin-left:2cm; line-height: 2.0;'>
			<li><a class='link' href='/data/download/clusters/fishes/UCNE_subclusters_fugu.xls'>Orthologous syntenic subclusters of UCNEs in <i>Fugu</i> <small>(excel table)</small></a></li>
			<li><a class='link' href='/data/download/clusters/fishes/UCNE_subclusters_medaka.xls'>Orthologous syntenic subclusters of UCNEs in medaka <small>(excel table)</small></a></li>
			<li><a class='link' href='/data/download/clusters/fishes/UCNE_subclusters_stickleback.xls'>Orthologous syntenic subclusters of UCNEs in stickleback <small>(excel table)</small></a></li>
			<li><a class='link' href='/data/download/clusters/fishes/UCNE_subclusters_tetraodon.xls'>Orthologous syntenic subclusters of UCNEs in <i>Tetraodon</i> <small>(excel table)</small></a></li>
			<li><a class='link' href='/data/download/clusters/fishes/UCNE_subclusters_zebrafish.xls'>Orthologous syntenic subclusters of UCNEs in zebrafish <small>(excel table)</small></a></li>
		</ul>
 </p>

<p>
	<hr/><p style='background-color: #d7dfe2;'><h2>Local UCNEbase installation</h2></p><hr/>
  		<p style='line-height: 2.0;'>You can create your own instance of a UCNEbase database by building one de novo.
  		To build the UCNEbase database from scratch, you will need a MySQL server and the UCNEbase database SQL source. Installation packages and documentation on the MySQL database itself can be found on the MySQL website.
  		The UCNEbase SQL source	can be found here:<br/>
  		<p style='margin-left:5cm; line-height:2.0'>
			<a class='link' href='./data/download/mysql_data/UCNEbase_v_2_0.sql.gz'><img src='./Icons/dload.png'> UCNEbase_v_2_0.sql.gz</a><br/>
			<a class='link' href='./data/download/mysql_data/UCNEbase_v_1_0.sql.gz'><img src='./Icons/dload.png'> UCNEbase_v_1_0.sql.gz</a><br/>
</p>

<!-- ######### Insert the footer #########-->
<?php readfile("footer.html"); ?>

</body>
</html>

