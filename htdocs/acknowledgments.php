<?php readfile("header.php"); ?>



<div style='border:0;width:80%'>
<h2>Acknowledgments</h2>

<ul>
<li>All genome assemblies and corresponding annotations were downloaded from the <a href='http://hgdownload.cse.ucsc.edu/downloads.html'>UCSC Genome Browser website</a>.
<br/>To see the credits for a specific genome assembly, please click <a href='http://genome.ucsc.edu/goldenPath/credits.html'>here</a>. </li>
<br>
<li>The computations were performed at the <a href='https://www.vital-it.ch'>Vital-IT </a> Center for high-performance computing of the <a href='https://www.sib.swiss/'>SIB Swiss Institute of Bioinformatics</a>. </li>
<br>
<li>Most of the species pictures were taken from the <a href='http://www.ensembl.org/index.html'> Ensembl Genome Browser</a>. </li>
<br>
</ul>
</div>

<!-- ######### Insert the footer #########-->
<?php readfile("footer.html"); ?>

</body>
</html>
