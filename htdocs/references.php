<?php readfile("header.php"); ?>


<br><br>

<div style='border:0;width:80%'>
<ul>
<li><a href="https://dx.doi.org/10.1093%2Fnar%2Fgks1092">UCNEbase - a database of ultra-conserved non-coding elements and genomic regulatory blocks.</a><br> Dimitrieva, S. and Bucher, P. <i>Nucleic Acids Res</i> (2013) </li>
<br>
<li><a href="https://doi.org/10.1093/bioinformatics/bts400">Genomic context analysis reveals dense interaction network between vertebrate ultra-conserved non-coding elements.</a><br> Dimitrieva, S. and Bucher, P. <i>Proceedings of ECCB12</i> (2012) </li>
<br>
<li><a href="http://www.biomedcentral.com/1471-2164/8/398/">Vertebrate conserved non coding DNA regions have a high persistence length and a short persistence time.</a><br> Retelska, D., Beaudoing, E., Notredame, C., Jongeneel, C.V. and Bucher, P. <i>BMC Genomics, 8, 398</i> (2007) </li>
<br>
</ul>
</div>

<!-- ######### Insert the footer #########-->
<?php readfile("footer.html"); ?>

</body>
</html>
