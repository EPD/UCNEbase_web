<?php require_once('config.inc.php'); ?>
<?php
	$data=$_GET["region"];
	$value=$_GET["value"];
	$con2 = mysqli_connect($config['database']['host'], $config['database']['user'], $config['database']['password']);
	if (!$con2){
  		die('Could not connect: ' . mysqli_connect_error());
 	}
	mysqli_select_db($con2, "UGRB");
	if ($data=="intragenic"){
		echo "<table class='ucnelist'>
			<tr><th width='100'>UCNE name</th><th width='50'>UCNE ID</th><th width='170'>Position</th><th width='60'>Type</th><th width='50'>Length</th>
			<th width='60'>Overlapping gene</th><th>Paralogs</th></tr>";
		$sql="SELECT * FROM ucne_hg19_details_coord c, ucne_names n, ucne_overlaping_genes g where c.id=n.id and c.id=g.id and g.gene='".$value."' order by chr, start ";
		$result = mysqli_query($con2, $sql);
		while($row = mysqli_fetch_array($result)){
  			echo " <tr>
  				<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["name"]."</a></td>
  				<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["id"]."</a></td>
  				<td><a class='link' href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["chr"].":".$row["start"]."-".$row["stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed'>".$row["chr"].":".$row["start"]."-".$row["stop"]."</a></td>
  				<td>".$row["type"]."</td>
  				<td>".$row["len"]."</td><td>".$row["gene"]."</td>";
				//find paralogs
				$paralogs="";
				$sql4="SELECT chr, start, stop, strand, evalue FROM ucne_paralogs where id=".$row["id"]." ";
				$result4 = mysqli_query($con2, $sql4);
				while($row4 = mysqli_fetch_array($result4)){
					$paralogs=$paralogs."<a class='link' href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row4["chr"].":".$row4["start"]."-".$row4["stop"]."'>".$row4["chr"].":".$row4["start"]."-".$row4["stop"]." (".$row4["strand"].")</a>; ";
				}
				echo "<td>".$paralogs."</td>";
  				echo "</tr>";
  		}
	} else {
		echo "<table class='ucnelist'>
			<tr><th width='100'>UCNE name</th><th width='50'>UCNE ID</th><th width='170'>Position</th><th width='60'>Type</th><th width='50'>Length</th>
			<th width='60'>Overlapping gene</th><th width='60'>Upstream gene</th><th width='60'>Downstream gene</th><th>Paralogs</th></tr>";
		$sql="SELECT u.*, name FROM ucne_hg19_details_coord u, refgene_hs19_v2 r, ucne_names n where r.gene='".$value."' and r.chrom=u.chr and u.start>(r.txStart-1000000) and u.stop<(r.txEnd+1000000) and u.id=n.id order by chr, start";
		$result = mysqli_query($con2, $sql);
		while($row = mysqli_fetch_array($result)){
			echo " <tr>
  				<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["name"]."</a></td>
  				<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["id"]."</a></td>
  				<td><a class='link' href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["chr"].":".$row["start"]."-".$row["stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed'>".$row["chr"].":".$row["start"]."-".$row["stop"]."</a></td>
  				<td>".$row["type"]."</td>
  				<td>".$row["len"]."</td>";
  			//find neighb. genes
  			$o_gene="";
  			$up_gene="";
  			$dw_gene="";
  			if ($row["type"]!="intergenic"){
  				$sql2="SELECT gene FROM ucne_overlaping_genes where id=".$row["id"]." ";
				$result2 = mysqli_query($con2, $sql2);
				while($row2 = mysqli_fetch_array($result2)){
					 $o_gene=$row2["gene"];
				}
			} else {
				$sql3="SELECT leftgene, rightgene FROM ucne_close_genes where id=".$row["id"]." ";
				$result3 = mysqli_query($con2, $sql3);
				while($row3 = mysqli_fetch_array($result3)){
					 $up_gene=$row3["leftgene"];
					 $dw_gene=$row3["rightgene"];
				}
			}
			echo "<td>".$o_gene."</td><td>".$up_gene."</td><td>".$dw_gene."</td>";
			//find paralogs
			$paralogs="";
			$sql4="SELECT chr, start, stop, strand, evalue FROM ucne_paralogs where id=".$row["id"]." ";
			$result4 = mysqli_query($con2, $sql4);
			while($row4 = mysqli_fetch_array($result4)){
				$paralogs=$paralogs."<a class='link' href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row4["chr"].":".$row4["start"]."-".$row4["stop"]."'>".$row4["chr"].":".$row4["start"]."-".$row4["stop"]." (".$row4["strand"].")</a>; ";
			}
			echo "<td>".$paralogs."</td>";
  			echo "</tr>";
		}
	}
	echo "</table>";



	mysqli_close($con2);
?>
