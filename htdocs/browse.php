<?php readfile("header.php"); ?>
<?php require_once('config.inc.php'); ?>


<div style='font-size: 12px; text-align: justify;'>
<p>
<?php
$data=$_GET["data"];
echo "
<div id='browseIndex'>";
  if ($data=="ucne"){
  	echo "<h2>Browse individual UCNEs</h2>
  		<p>
  			By clicking on the links below you will obtain a list of individual Ultraconserved Non-Coding Elements (UCNEs) based on a chromosomal location.
  		</p> ";
  } else {
  	echo "<h2>Browse Ultraconserved Genomic Regulatory Blocks (UCNE clusters)</h2>
  		<p>
  			By clicking on the links below you will obtain a list of Ultraconserved Genomic Regulatory Blocks (clusters of UCNEs) based on a selected criteria.
  		</p> ";
  }

echo  "<hr />
  <br/>

  <table summary='Browse the database' >
    <tbody>
      <tr>
        <td class='type'>By chromosomal location<br/>in the Human genome (hg19)</td>
        <td class='links'>";
			$con = mysqli_connect($config['database']['host'], $config['database']['user'], $config['database']['password']);
			if (!$con){
  				die('Could not connect: ' . mysqli_connect_error());
 			}
			mysqli_select_db($con, "UGRB");
			if ($data=="ucne"){
				$sql="SELECT chr FROM chr_order where specie='hg19' order by chr_order, chr ";
			} else {
				$sql="SELECT chr FROM chr_order where specie='hg19' and chr in (select hs_chr from clusters) order by chr_order, chr ";
			}
			$result = mysqli_query($con, $sql);
			while($row = mysqli_fetch_array($result)){
  				echo " <a class='link_' href='./list.php?data=".$data."&org=hg19&view=chr&value=".$row["chr"]."'>".$row["chr"]."</a>";
  			}

  		echo "</td>
       <td width=10%></td>
      </tr>
      <tr><td> <hr></td><td><hr> </td></tr>";
      if ($data=="cluster"){
      	echo "<tr>
      	<td class='type'>All UCNE clusters</td>
        <td class='links'>
        	<a class='link_' href='./list.php?data=".$data."&org=hg19&view=allBySize'>ordered by cluster size</a><br/>
        	<a class='link_' href='./list.php?data=".$data."&org=hg19&view=allByName'>ordered alphabetically</a>
        </td>
        </tr>
        <tr><td> <hr></td><td><hr> </td></tr>";
      }
      echo "<tr>";
      if ($data=="ucne"){
        echo "<td class='type'>By proximity of a gene<br/>in the Human genome (hg19)</td>
        <td class='links'>
        	Most enriched genes: <br/>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=AP3B1'>AP3B1</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=AUTS2'>AUTS2</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=DACH1'>DACH1</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=DACH2'>DACH2</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=EBF1'>EBF1</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=EBF3'>EBF3</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=EHBP1'>EHBP1</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=ESRRG'>ESRRG</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=FOXP1'>FOXP1</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=FOXP2'>FOXP2</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=LRBA'>LRBA</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=MEIS1'>MEIS1</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=MEIS2'>MEIS2</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=NBEA'>NBEA</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=NPAS3'>NPAS3</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=OLA1'>OLA1</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=PBX3'>PBX3</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=POLA1'>POLA1</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=SATB1'>SATB1</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=SOX6'>SOX6</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=TCF7L2'>TCF7L2</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=ZEB2'>ZEB2</a>
        	<a class='link_' href='./list.php?data=ucne&org=hg19&view=gene&value=ZFPM2'>ZFPM2</a>
        	<br/>
        	or<br/>
        	<form action='list.php' method='get'>
        		<input type='text' value='Type a gene name ...' onfocus=\"if(this.value==this.defaultValue) this.value='';\" name='value' size='25' maxlength='1024'/>
        		<input type='hidden' name='data' value='ucne'>
        		<input type='hidden' name='org' value='hg19'>
        		<input type='hidden' name='view' value='gene'>
        		<input type='submit' value='Search'/><br/>
        	</form>
        </td>";
     } else {
    	echo "<td class='type'>By gene association<br/>in the Human genome (hg19)</td>
        <td class='links'>
        	Ultraconserved Gene Regulatory Blocks containing putative target genes: <br/>";
        	$top25_genes=""; $rest_genes="";
        	$sql1="SELECT name, count(*) as cnt FROM clusters_repr_names c, ucne_to_clusters uc where uc.cluster_id=c.cluster_id group by name order by cnt desc limit 0,25";
        	$sql2="SELECT name, count(*) as cnt FROM clusters_repr_names c, ucne_to_clusters uc where uc.cluster_id=c.cluster_id group by name order by cnt desc limit 25, 250";
        	$result1 = mysqli_query($con, $sql1);
			while($row1 = mysqli_fetch_array($result1)){
  				$top25_genes=$top25_genes."<a class='link_' href='./view.php?data=cluster&entry=".$row1["name"]."'>".$row1["name"]." <small>(".$row1["cnt"].")</small></a> ";
  			}
  			$result2 = mysqli_query($con, $sql2);
			while($row2 = mysqli_fetch_array($result2)){
  				$rest_genes=$rest_genes."<a class='link_' href='./view.php?data=cluster&entry=".$row2["name"]."'>".$row2["name"]." <small>(".$row2["cnt"].")</small></a> ";
  			}
  			echo $top25_genes;
  			echo "<a id='displayGenesText' class='link_' href='javascript:toggleGenes();'><img src='./Icons/plus.gif'> <small>show all</small></a><br/>
			<div id='toggleGenesText' style='display: none'>".$rest_genes."</div>";


        	echo "or<br/>
        	<form action='view.php' method='get'>
        		<input type='text' value='Type a gene name ...' onfocus=\"if(this.value==this.defaultValue) this.value='';\" name='entry' size='25' maxlength='1024'/>
        		<input type='hidden' name='data' value='cluster'>
        		<input type='submit' value='Search'/><br/>
        	</form>
        	<small>* the numbers in brackets denote the number of UCNEs forming the cluster</small>
        </td>";
     }
     mysqli_close($con);
        echo "<td width=10%></td>

      </tr>
    </tbody>
  </table>
  <br/>
  <hr />

</div>";?>
</p>
<p>

</p>

</div>




<!-- ######### Insert the footer #########-->
<?php readfile("footer.html"); ?>





</body>
</html>
