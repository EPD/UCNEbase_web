<?php


// ---------- misc functions ----------

function get_config_value($path, $config) {
  $segments = explode(':', $path);
  $section = $config;
  $value = $path;
  foreach ($segments as $key) {
    if (array_key_exists($key, $section)) {
      if (is_array($section[$key])) {
        $section = $section[$key];
      }
      else {
        $value = $section[$key];
        break;
      }
    }
  }
  return $value;
}

function expand_config_variables($section, $config) {
  foreach($section as $key=>$value) {
    if (is_array($value)) {
      // dealing with a section
      $section[$key] = expand_config_variables($value, $config);
    }
    else {
      if (preg_match_all('/\$\{([^\}]*)\}/', $value, $matches, PREG_SET_ORDER)) {
        foreach ($matches as $match) {
          $expanded_value = get_config_value($match[1], $config);
          if ($expanded_value != $match[1]) {
            $value = str_replace($match[0], $expanded_value, $value);
          }
        }
        $section[$key] = $value;
      }
    }
  }
  return $section;
}

function load_config($config_file)
{
  if (!file_exists($config_file)) {
    die("Configuration file '$config_file' not found!");
  }
  $config = parse_ini_file($config_file, true, INI_SCANNER_RAW);

  // If local config file exists, parse and merge it with the global config
  $local_config_file = preg_replace('/.ini$/', '.local.ini', $config_file);
  if (file_exists($local_config_file)) {
    $local_config = parse_ini_file($local_config_file, true, INI_SCANNER_RAW);

    // merge name=value config settings
    $merged_config = array_merge($config, $local_config);
    foreach($config as $key=>$value) {
        if (is_array($value) && (array_key_exists($key, $local_config) && is_array($local_config[$key]))) {
          // merge netsted config sections, e.g. [classname]
          $merged_config[$key] = array_merge($value, $local_config[$key]);
        }
    }
    $config = $merged_config;
  }

  // expand variables
  $config = expand_config_variables($config, $config);

  return $config;
}

// Load and merge config files.
global $config;
$config_file = getenv('CONFIG_FILE');
if (! $config_file) {
  // Assume config is at ./config/config.ini
  $config_file = './config/config.ini';
}
$config = load_config($config_file);
//print_r($config);

?>
