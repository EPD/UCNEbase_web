<?php readfile("header.php"); ?>
<?php require_once('config.inc.php'); ?>


<!-- <div style='font-size: 12px; text-align: justify;'> -->
<div align="center" style=" margin: 0 auto; border : solid 1px #CCCCCC; padding : 4px;">
<?php
	$data=$_GET["data"];
	$id=$_GET["entry"];
        $newid = filter_var($id, FILTER_SANITIZE_STRING);
	$con = mysqli_connect($config['database']['host'], $config['database']['user'], $config['database']['password']);
	if (!$con){
  		die('Could not connect: ' . mysqli_connect_error());
 	}
	mysqli_select_db($con, "UGRB");
	if ($data=="ucne"){
		if (!is_numeric($newid)) {
			$name=$newid;
			$sql0="SELECT id FROM ucne_names where name='".$name."' ";
			$result0 = mysqli_query($con, $sql0);
			if($row0 = mysqli_fetch_array($result0)) $newid=$row0["id"];
		} else {
			$sql="SELECT name FROM ucne_names where id=".$newid." ";
			$result = mysqli_query($con, $sql);
			if($row = mysqli_fetch_array($result)) $name=$row["name"];
		}
		echo "<h3>UCNE: <FONT COLOR='#C00000 '>".$name."</FONT> <br/>(id = ".$newid.")</h3>";
		$sql="SELECT d.chr, d.start, d.stop, len, replace(replace(type,'UTR3','UTR'),'UTR5','UTR') as type, fasta FROM ucne_hg19_details_coord d, ucne_fasta f where d.id=".$newid." and d.id=f.id and f.specie='hs19' and d.specie='hg19'";
		$result = mysqli_query($con, $sql);
		while($row = mysqli_fetch_array($result)){
			echo "<table style='width: 99%; margin: 0 auto; font-size: 12px; font-family: Helvetica;' border='0'>
				<tbody>
				<tr align='left' style='background-color: #D7DFE2;'><td align='left' colspan='2'><b>General information about the entry</b></td></tr>
				<tr align='left'><td width='200px'>Entry location:</td><td>".$row["type"]."</td></tr>
				<tr align='left'><td>Position:</td><td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&amp;clade=mammal&amp;org=Human&amp;db=hg19&amp;position=".$row["chr"].":".$row["start"]."-".$row["stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/UCNEs_hg19.bed' target='_blank'>".$row["chr"].":".$row["start"]."-".$row["stop"]." <img src='./Icons/view.png'></a></td></tr>
				<tr align='left'><td>Length:</td><td>".$row["len"]." nt</td></tr>
				<tr align=left><td>Sequence:</td><td><a class='link' href='./data/fasta/hg19/".$newid.".fasta'><img src='./Icons/dload.png'>".$name.".fasta </a></td></tr>";
				if ($row["type"]!="intergenic"){
  					$sql2="SELECT gene FROM ucne_overlaping_genes where id=".$newid." ";
					$result2 = mysqli_query($con, $sql2);
					while($row2 = mysqli_fetch_array($result2)){
						echo "<tr align=left><td>Overlapping gene:</td><td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row2["gene"]."' target='_blank'>".$row2["gene"]."</a></td></tr>";
					}
				} else {
					$sql3="SELECT leftgene, rightgene FROM ucne_close_genes where id=".$newid." ";
					$result3 = mysqli_query($con, $sql3);
					while($row3 = mysqli_fetch_array($result3)){
						echo "<tr align=left><td>Upstream gene:</td><td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row3["leftgene"]."' target='_blank'>".$row3["leftgene"]."</a></td></tr>";
						echo "<tr align=left><td>Downstream gene:</td><td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row3["rightgene"]."' target='_blank'>".$row3["rightgene"]."</a></td></tr>";
					}
				}
				$paralogs="";
				$sql4="SELECT chr, seq_start, seq_stop, evalue, paralog_ucne FROM ucne_paralogs where id=".$newid." and evalue<0.0001 order by chr, seq_start ";
				$result4 = mysqli_query($con, $sql4);
				while($row4 = mysqli_fetch_array($result4)){
					$arr_par_ucne=explode(";", $row4["paralog_ucne"]);
					$paralogs=$paralogs."<a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row4["chr"].":".$row4["seq_start"]."-".$row4["seq_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/UCNEs_hg19.bed' target='_blank'>".$row4["chr"].":".$row4["seq_start"]."-".$row4["seq_stop"]."</a>";
					if ($row4["paralog_ucne"]!="") $paralogs=$paralogs.": ";
					foreach ($arr_par_ucne as $par_ucne)
  					{
  						$par_ucne_name="";
  						if ($par_ucne!=""){
 							$sql4n="SELECT name FROM ucne_names where id=".$par_ucne." ";
							$result4n = mysqli_query($con, $sql4n);
							if($row4n = mysqli_fetch_array($result4n)) $par_ucne_name=$row4n["name"];
 							$paralogs=$paralogs." <a class='link' href='./view.php?data=ucne&entry=".$par_ucne."'>".$par_ucne_name."</a>; ";
 						}
 					}
 					$paralogs=$paralogs."<br/>";
				}
				echo "<tr align='left' valign='top'><td>Paralogs:</td><td>".$paralogs."</td></tr>";

				$sql4="SELECT c.cluster_id, n.name , hs_chr, hs_start, hs_stop FROM ucne_to_clusters u, clusters c, clusters_repr_names n
					where u.ucne_id=".$newid." and u.cluster_id=c.cluster_id and u.cluster_id=n.cluster_id ";
				$result4 = mysqli_query($con, $sql4);
				$cluster_name="";
				while($row4 = mysqli_fetch_array($result4)){
					 $cluster_name=$row4["name"];
					 echo "<tr align='left' valign='center'><td>Ultraconserved Genomic Regulatory Block (UGRB):</td>
					 <td><a class='link' href='./view.php?data=cluster&entry=".$row4["cluster_id"]."'>".$row4["name"]."_cluster</a>: <a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row4["hs_chr"].":".$row4["hs_start"]."-".$row4["hs_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/UCNEs_hg19.bed' target='_blank'>".$row4["hs_chr"].":".$row4["hs_start"]."-".$row4["hs_stop"]." <img src='./Icons/view.png'></a></td></tr>";
				}
		//External links
				echo "<tr align='left' ><td align='left' colspan='2' style='background-color: #D7DFE2;'><b>External links:</b></td></tr>
				<tr align='left'><td>CONDOR database:</td><td>";
				$sql6="SELECT * FROM condor_ucne_map where ucne_id=".$newid." ";
				$result6 = mysqli_query($con, $sql6);
				while($row6 = mysqli_fetch_array($result6)){
					echo "<a href='http://condor.nimr.mrc.ac.uk/cgi-bin/show_cne.cgi?cneid=".$row6["condor_id"]."' target='_blank'>".$row6["condor_id"]."</a>; ";
				}
				echo "</td></tr>";
				echo "<tr align='left'><td>VISTA Enhancer Browser:</td><td>";
				$sql6="SELECT num FROM vista_ucne_map v, vista_coord c where ucne_id=".$newid." and v.vista_id=c.vista_id ";
				$result6 = mysqli_query($con, $sql6);
				while($row6 = mysqli_fetch_array($result6)){
					echo "<a href='http://enhancer.lbl.gov/cgi-bin/imagedb3.pl?form=presentation&show=1&experiment_id=".$row6["num"]."&organism_id=1' target='_blank'>hs".$row6["num"]."</a>; ";
				}
				echo "</td></tr>";
				echo "<tr align='left'><td>UCE (Bejerano et al. 2004):</td><td>";
				$sql6="SELECT * FROM uce_ucne_map where ucne_id=".$newid." ";
				$result6 = mysqli_query($con, $sql6);
				$r=0;
				while($row6 = mysqli_fetch_array($result6)){
					echo $row6["uce_id"]." ";
					$r++;
				}
				if ($r>0) echo "<a href='http://users.soe.ucsc.edu/~jill/ultra.html' target='_blank'>http://users.soe.ucsc.edu/~jill/ultra.html</a>";
				echo "</td></tr>
				<tr align='left'><td>Other browsers view:</td><td><a href='http://www.ensembl.org/Homo_sapiens/Location/View?r=".str_replace("chr","",$row["chr"]).":".$row["start"]."-".$row["stop"]."' target='_blank'>[Ensembl]</a>
				<a href='http://ecrbrowser.dcode.org/xB.php?db=hg19&location=".$row["chr"].":".$row["start"]."-".$row["stop"]."' target='_blank'>[ECR browser]</a>
				<a href='http://ancora.genereg.net/cgi-bin/gbrowse/hg19/?start=".$row["start"].";stop=".$row["stop"].";ref=".$row["chr"]."' target='_blank'>[Ancora]</a>
				</td></tr>";

	 //Conservation in chicken
				echo "<tr align='left' ><td align='left' colspan='2' style='background-color: #D7DFE2;'><b>Conservation in chicken:</b></td></tr>";
				$sql5="SELECT chr, start, stop FROM ucne_coord u where specie='gg3' and id=".$newid." ";
				$result5 = mysqli_query($con, $sql5);
				echo "<tr align='left'><td width='200px'><img src='./images_species/thumb_Chicken.png' align='middle' /> Chicken (galGal3):</td><td> ";
				while($row5 = mysqli_fetch_array($result5)){
					echo "Region: <a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&amp;clade=vertebrate&amp;org=Chicken&amp;db=galGal3&amp;position=".$row5["chr"].":".$row5["start"]."-".$row5["stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/UCNEs_gg3.bed' target='_blank'>".$row5["chr"].":".$row5["start"]."-".$row5["stop"]."</a>";
				}
				echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspIdentity: >95%";
				$p_reg=0;
				$p_regions="";
				$sql5p="SELECT hit, identity, chr, seq_start, seq_stop, evalue, bitscore FROM whole_genome_aln_hits where id=".$newid." and specie='gg3' and evalue<0.0001 and paralog_hit=3 order by evalue";
				$result5p = mysqli_query($con, $sql5p);
				while($row5p = mysqli_fetch_array($result5p)){
					$p_reg++;
					$p_regions=$p_regions."<table><tr><td style='padding-left: 20px; text-align:left'>Paralog #".$p_reg.":&nbsp&nbsp&nbsp<a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&amp;clade=vertebrate&amp;org=Chicken&amp;db=galGal3&amp;position=".$row5p["chr"].":".$row5p["seq_start"]."-".$row5p["seq_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/UCNEs_gg3.bed'>".$row5p["chr"].":".$row5p["seq_start"]."-".$row5p["seq_stop"]."</a>&nbsp&nbsp&nbspIdentity: ".$row5p["identity"]."%
					&nbsp&nbsp&nbspE-value: ".$row5p["evalue"]."&nbsp&nbsp&nbspBit-score: ".$row5p["bitscore"]."</td></tr></table>";
				}
				if ($p_reg>0) echo "<table><tr><td><i>";
				if ($p_reg==1) echo "This UCNE has ".$p_reg." paralogous region in Chicken ";
				if ($p_reg>1) echo "This UCNE has ".$p_reg." paralogous regions in Chicken ";
				if ($p_reg>0) {
					echo "</i><a id='displayImg100' href='javascript:toggleParalogs(100);' class='link'><img src='./Icons/plus.gif'></a>
							<div id='toggleParalogsText100' style='display: none'>".$p_regions."</div>";
					echo "</td></tr></table>";
				}
				echo "<br/></td></tr>";



	//Conservation in other species
				echo "<tr align='left' ><td align='left' colspan='2' style='background-color: #D7DFE2;'><b>Conservation in other species:</b></td></tr>";
				$species=array("Mouse", "Armadillo", "Opossum", "Platypus", "Zebra_finch", "Lizard", "Painted_turtle",  "Xenopus", "Fugu","Medaka","Stickleback","Tetraodon","Zebrafish", "Lamprey", "Ciona_intestinalis", "Sea_urchin", "Lancelet");
				$assembly=array("mm10", "dasNov1","monDom5", "ornAna1", "taeGut1", "anoCar2", "chrPic1", "xenTro3",  "fr2","oryLat2","gasAcu1", "tetNig2","danRer7", "petMar1", "ci2", "strPur2", "braFlo1");
				for($i=0;$i<count($species);$i++){
					$reg=0;
  					$sql5="SELECT hit, identity, chr, seq_start, seq_stop, evalue, bitscore FROM whole_genome_aln_hits where id=".$newid." and specie='".$species[$i]."' and evalue<0.0001 and paralog_hit is null order by evalue";
					$result5 = mysqli_query($con, $sql5);
					echo "<tr align='left'><td ><img src='./images_species/thumb_".$species[$i].".png' align='middle' /> ".$species[$i]." (".$assembly[$i]."):</td><td> ";
					while($row5 = mysqli_fetch_array($result5)){
						$reg++;
						echo "<table>";
				//		if (($species[$i]!="Fugu")&&($species[$i]!="Medaka")&&($species[$i]!="Stickleback")&&($species[$i]!="Tetraodon")&&($species[$i]!="Zebrafish"))
							echo "<tr><td>Region #".$reg.":&nbsp&nbsp&nbsp<a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&amp;clade=vertebrate&amp;org=".$species[$i]."&amp;db=".$assembly[$i]."&amp;position=".$row5["chr"].":".$row5["seq_start"]."-".$row5["seq_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/UCNEs_".$assembly[$i].".bed' target='_blank'>".$row5["chr"].":".$row5["seq_start"]."-".$row5["seq_stop"]."</a>&nbsp&nbsp&nbspIdentity: ".$row5["identity"]."%
							&nbsp&nbsp&nbspE-value: ".$row5["evalue"]."&nbsp&nbsp&nbspBit-score: ".$row5["bitscore"]." ";
				//		else
				//		echo "<tr><td>Region #".$reg.":&nbsp&nbsp&nbsp<a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&amp;clade=vertebrate&amp;org=".$species[$i]."&amp;db=".$assembly[$i]."&amp;position=".$row5["chr"].":".$row5["seq_start"]."-".$row5["seq_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/subclusters/".$species[$i]."_subclusters/cluster_".$cluster_name."_subclusters_".$assembly[$i].".bed'>".$row5["chr"].":".$row5["seq_start"]."-".$row5["seq_stop"]."</a>&nbsp&nbsp&nbspIdentity: ".$row5["identity"]."%
				//		&nbsp&nbsp&nbspE-value: ".$row5["evalue"]."&nbsp&nbsp&nbspBit-score: ".$row5["bitscore"]." ";
						echo "</td></tr>";
						echo "</table>";
					}
					$p_reg=0;
					$p_regions="";
					$sql5p="SELECT hit, identity, chr, seq_start, seq_stop, evalue, bitscore FROM whole_genome_aln_hits where id=".$newid." and specie='".$species[$i]."' and evalue<0.0001 and (paralog_hit=1 or paralog_hit=3) order by evalue";
					$result5p = mysqli_query($con, $sql5p);
					while($row5p = mysqli_fetch_array($result5p)){
						$p_reg++;
						$p_regions=$p_regions."<table><tr><td style='padding-left: 20px; text-align:left'>Paralog #".$p_reg.":&nbsp&nbsp&nbsp<a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&amp;clade=vertebrate&amp;org=".$species[$i]."&amp;db=".$assembly[$i]."&amp;position=".$row5p["chr"].":".$row5p["seq_start"]."-".$row5p["seq_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/UCNEs_".$assembly[$i].".bed'>".$row5p["chr"].":".$row5p["seq_start"]."-".$row5p["seq_stop"]."</a>&nbsp&nbsp&nbspIdentity: ".$row5p["identity"]."%
						&nbsp&nbsp&nbspE-value: ".$row5p["evalue"]."&nbsp&nbsp&nbspBit-score: ".$row5p["bitscore"]."</td></tr></table>";
					}
					if ($p_reg>0) echo "<table><tr><td><i>";
					if ($p_reg==1) echo "This UCNE has ".$p_reg." paralogous region in ".$species[$i]." ";
					if ($p_reg>1) echo "This UCNE has ".$p_reg." paralogous regions in ".$species[$i]." ";
					if ($p_reg>0) {
						echo "</i><a id='displayImg".$i."' href='javascript:toggleParalogs(".$i.");' class='link'><img src='./Icons/plus.gif'></a>
								<div id='toggleParalogsText".$i."' style='display: none'>".$p_regions."</div>";
						echo "</td></tr></table>";
					}
					echo "<br/></td></tr>";
  				}

				echo "</tbody></table>";
  		}
  		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	} else if ($data=="cluster"){
		$view=$_GET["view"];
	  	if ($view=="by_ucne"){
	  		$sql0="SELECT cluster_id FROM ucne_to_clusters uc, ucne_names n where uc.ucne_id=n.id and (ucne_id='".$newid."' or name='".$newid."')";
	  		$result0 = mysqli_query($con, $sql0);
			if($row0 = mysqli_fetch_array($result0)) $newid=$row0["cluster_id"];
	  	}
		if (!is_numeric($newid)) {
			$sql0="SELECT cluster_id FROM cluster_genes where specie='hg19' and gene='".$newid."'";
			$result0 = mysqli_query($con, $sql0);
			if($row0 = mysqli_fetch_array($result0)) $newid=$row0["cluster_id"];
			else {
				$sql0="SELECT cluster_id FROM clusters_repr_names where name='".$newid."' or name=replace('".$newid."', '_cluster','') ";
				$result0 = mysqli_query($con, $sql0);
				if($row0 = mysqli_fetch_array($result0)) $newid=$row0["cluster_id"];
			}
		}
		$sql="SELECT c.cluster_id, n.name, hs_chr, hs_start, hs_stop, gg3_chr, gg3_start, gg3_stop, strand FROM clusters c, clusters_repr_names n where c.cluster_id=".$newid." and c.cluster_id=n.cluster_id";
		$result = mysqli_query($con, $sql);
		while($row = mysqli_fetch_array($result)){
			echo "<h3>Ultraconserved Genomic Regulatory Block: <FONT COLOR='#C00000 '>".$row["name"]."_cluster</FONT><br/>(id = ".$newid.") </h3>";
			echo "<table style='width: 99%; margin: 0 auto; font-size: 12px; font-family: Helvetica;' border='0'>
				<tbody>
				<tr align='left' style='background-color: #D7DFE2;'><td align='left' colspan='2'><b>General information about the entry</b></td></tr>
				<tr align='left'><td>Representative name:</td><td><b>".$row["name"]."_cluster</b></td></tr>
				<tr align='left'><td width='200px'>Position:</td><td><a href=\"http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/UCNEs_hg19.bed\" target='_blank'>".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]." <img src='./Icons/view.png'></a></td></tr>";
			$sql1="SELECT count(*) as c FROM ucne_to_clusters where cluster_id=".$newid." ";
			$result1 = mysqli_query($con, $sql1);
			while($row1 = mysqli_fetch_array($result1)){
				echo "<tr align='left'><td># UCNEs:</td><td>".$row1["c"]."</td></tr>";
			}
			$genes=""; $targets="";
  			$sql2="select gene, target_gene from cluster_genes where cluster_id=".$newid." and specie='hg19' ";
			$result2 = mysqli_query($con, $sql2);
			while($row2 = mysqli_fetch_array($result2)){
				$genes=$genes."<a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row2["gene"]."' target='_blank'>".$row2["gene"]."</a>; ";
				if ($row2["target_gene"]==1) $targets=$targets."<a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row2["gene"]."' target='_blank'>".$row2["gene"]."</a>; ";
			}
			echo "<tr align='left'><td>Assiciated genes:</td><td>".$genes."</td></tr>";
			echo "<tr align='left'><td>Possible target genes:</td><td>".$targets."</td></tr>";
			$sql4="SELECT ucne_id, name FROM ucne_to_clusters u, ucne_names n where cluster_id=".$newid." and u.ucne_id=n.id";
			$result4 = mysqli_query($con, $sql4);
			$ucnes="";
			while($row4 = mysqli_fetch_array($result4)){
				$ucnes=$ucnes."<a class='link' href='./view.php?data=ucne&entry=".$row4["ucne_id"]."'>".$row4["name"]."</a>; ";
			}
			echo "<tr align='left' valign='top'><td>UCNEs forming the cluster:</td><td>".$ucnes."</td></tr>";
			$sql4="SELECT c2.cluster_id as c_id, name FROM cluster_paralog_groups c1, cluster_paralog_groups c2, clusters_repr_names n
			where c1.cluster_id=".$newid." and c1.group_id=c2.group_id and c1.cluster_id<>c2.cluster_id and c2.cluster_id=n.cluster_id order by name";
			$result4 = mysqli_query($con, $sql4);
			$paralogClusters="";
			while($row4 = mysqli_fetch_array($result4)){
				$paralogClusters=$paralogClusters."<a class='link' href='./view.php?data=cluster&entry=".$row4["c_id"]."'>".$row4["name"]."_cluster</a>; ";
			}
			echo "<tr align='left' valign='top'><td>Paralogous cluster(s):</td><td>".$paralogClusters."</td></tr>";

			echo "<tr align='left'><td></td><td><br/>
			<a id='displayGenesText2' class='link_' href='javascript:toggleGenes2();'><img src='./Icons/show_img.png'> <small>View cluster image</small></a><br/>
			<div id='toggleGenesText2' style='display: none'><br/><a href=\"http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/cluster_bed/cluster_".$row["name"]."_hg19.bed\" target='_blank'><img src='./data/cluster_images/".$row["name"].".png'></a></div>
			</td></tr>";

			echo "<tr align='left' ><td align='left' colspan='2' style='background-color: #D7DFE2;'><b>Conservation in chicken:</b></td></tr>";
			echo "<tr align='left'><td width='200px'><img src='./images_species/thumb_Chicken.png' align='middle' /> Chicken (galGal3):</td>
				<td> Position: <a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&amp;clade=vertebrate&amp;org=Chicken&amp;db=galGal3&amp;position=".$row["gg3_chr"].":".$row["gg3_start"]."-".$row["gg3_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/UCNEs_gg3.bed' target='_blank'>".$row["gg3_chr"].":".$row["gg3_start"]."-".$row["gg3_stop"]."</a><br/>";
			$genes="";
  			$sql2="select gene from cluster_genes where cluster_id=".$newid." and specie='gg3' ";
			$result2 = mysqli_query($con, $sql2);
			while($row2 = mysqli_fetch_array($result2)){
				$genes=$genes."<a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row2["gene"]."' target='_blank'>".$row2["gene"]."</a>; ";
			}
			echo "Associated genes: ".$genes."</td></tr>";
			echo "<tr align='left' ><td align='left' colspan='2' style='background-color: #D7DFE2;'><b>Conservation in other species:</b></td></tr>";
			$species=array("Mouse","Opossum","Platypus","Zebra_finch","Lizard", "Painted_turtle","Xenopus","Fugu","Medaka","Stickleback","Tetraodon","Zebrafish");
			$assembly=array("mm10","monDom5","ornAna1","taeGut1","anoCar2","chrPic1","xenTro3","fr2","oryLat2","gasAcu1","tetNig2","danRer7");
			for($i=0;$i<count($species);$i++){

  				$sql5="SELECT specie_cluster_id, chr, start, stop, paralog
  				FROM clusters_species where cluster_id=".$newid." and specie='".$species[$i]."' and paralog is null ";
				$result5 = mysqli_query($con, $sql5);
				echo "<tr align='left' border=1><td><img src='./images_species/thumb_".$species[$i].".png' align='middle' /> ".$species[$i]." (".$assembly[$i]."):</td><td> ";
				$k=0;
				while($row5 = mysqli_fetch_array($result5)){
					$k++;
					echo "<table><a name='".$species[$i]."'>";
					echo "<tr><td  valign='top' width='80px'>Subcluster #".$k."</td>
					<td><table><tr><td width='60px'><br/>Position: </td><td><br/><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&amp;clade=vertebrate&amp;org=".$species[$i]."&amp;db=".$assembly[$i]."&amp;position=".$row5["chr"].":".$row5["start"]."-".$row5["stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/UCNEs_".$assembly[$i].".bed' target='_blank'>".$row5["chr"].":".$row5["start"]."-".$row5["stop"]." <img src='./Icons/view.png'></a></td></tr>";
					$sql6="SELECT distinct c.ucne_id, n.name FROM clusters_species_ucne_hits c, whole_genome_aln_hits w, ucne_names n where
						c.cluster_id=".$newid." and c.specie_cluster_id=".$row5["specie_cluster_id"]." and c.specie='".$species[$i]."'
						and c.ucne_id=w.id and c.hit=w.hit and evalue<0.0001 and c.ucne_id=n.id";
					$result6 = mysqli_query($con, $sql6);
					$num_ucnes=0;
					$subcl_ucnes="";
					while($row6 = mysqli_fetch_array($result6)){
						$num_ucnes++;
						$subcl_ucnes=$subcl_ucnes."<a class='link' href='./view.php?data=ucne&entry=".$row6["ucne_id"]."'>".$row6["name"]."</a>; ";
					}
					echo "<tr><td># UCNEs:</td><td>".$num_ucnes."</td></tr>";
					echo "<tr><td valign='top'>UCNEs:</td><td>".$subcl_ucnes."</td></tr>";
					echo "</table></td></tr>";
					echo "</table>";
				}
				echo "</td></tr> ";
				echo "<tr><td><hr></td><td><hr></td></tr>";
  			}
		echo "</tbody></table>";
		}
	} //end data=cluster


?>


<p>

</p>
<p>

</p>

</div>




<!-- ######### Insert the footer #########-->
<?php readfile("footer.html"); ?>





</body>
</html>
