<?php require_once('config.inc.php'); ?>
<?php
$searchTerm=$_GET["searchTerm"];
if ($searchTerm=="ucne"){
    echo "<table class='tblSearch' >
    <tbody>
      <th><td style='padding: 0px;'></td><td style='padding: 0px;'></td><td width=10% style='padding: 0px;'></td></th>
      <tr>
        <td class='type'>Overlapping with a chromosomal location in a species</td>
        <td class='links'>
        	<form action='list.php' method='get'>
        		<input type='hidden' name='data' value='ucne'>
        		<input type='hidden' name='view' value='region'>
				Specie: <select name='org'>
					<option value='hg19'>Human (hg19)</option>
					<option value='galGal3'>Chicken (galGal3)</option>
					<option value='fr2'>Fugu (fr2)</option>
					<option value='oryLat2'>Medaka (oryLat2)</option>
					<option value='gasAcu1'>Stickleback (gasAcu1)</option>
					<option value='tetNig2'>Tetraodon (tetNig2)</option>
					<option value='danRer7'>Zebrafish (danRer7)</option>
				</select><br/>
				Chromosome: <input type='text' value='chr1' onfocus='if(this.value==this.defaultValue) this.value='';' name='chr' size='10' maxlength='1024'/>
				Region from <input type='text' value='10000000' onfocus='if(this.value==this.defaultValue) this.value='';' name='start' size='10' maxlength='1024'/>
				to <input type='text' value='10800000' onfocus='if(this.value==this.defaultValue) this.value='';' name='stop' size='10' maxlength='1024'/>
        		<input type='submit' value='Search' class='ucnesubmit'/><br/>
			</form>
		</td>
      </tr>
      <tr>
      	<td class='type'>Located within a gene</td>
        <td class='links'>
        	<form action='list.php' method='get'>
        		<input type='hidden' name='data' value='ucne'>
        		<input type='hidden' name='org' value='hg19'>
        		<input type='hidden' name='view' value='gene'>
        		<input type='text' value='Type a gene name ...' onfocus=\"if(this.value==this.defaultValue) this.value='';\" name='value' size='25' maxlength='1024'/>
        		<input type='submit' value='Search' class='ucnesubmit'/><br/>
        	</form>
        </td>
      </tr>
      <tr>
      	<td class='type'>In proximity of a gene</td>
        <td class='links'>
        	<form action='list.php' method='get'>
        		<input type='hidden' name='data' value='ucne'>
        		<input type='hidden' name='org' value='hg19'>
        		<input type='hidden' name='view' value='flank_gene'>
        		Flanking distance (bps) <input type='text' value='1000000' onfocus=\"if(this.value==this.defaultValue) this.value='';\" name='flank' size='10' maxlength='1024'/>
        		of the gene: <input type='text' value='Type a gene name ...' onfocus=\"if(this.value==this.defaultValue) this.value='';\" name='gene' size='20' maxlength='1024'/>
        		<input type='submit' value='Search' class='ucnesubmit'/><br/>
        	</form>
        </td>
      </tr>
      <tr>
      	<td class='type'>UCNEs longer than</td>
        <td class='links'>
        	<form action='list.php' method='get'>
        		<input type='hidden' name='data' value='ucne'>
        		<input type='hidden' name='org' value='hg19'>
        		<input type='hidden' name='view' value='bylength'>
        		length <input type='text' value='1000' onfocus=\"if(this.value==this.defaultValue) this.value='';\" name='value' size='10' maxlength='1024'/> bps
        		<input type='submit' value='Search' class='ucnesubmit'/><br/>
        	</form>
        </td>
        <td width=10%></td>
     </tr>
    </tbody>
  </table> ";
  } else if ($searchTerm=="cluster"){
  	echo "<table summary='Search the database' class='tblSearch' >
    <tbody>
      <th><td style='padding: 0px;'></td><td style='padding: 0px;'></td><td width=10% style='padding: 0px;'></td></th>
      <tr>
        <td class='type'>Overlapping with a chromosomal location in a species</td>
        <td class='links'>
        	<input type='hidden' name='data' value='cluster'>
        	<form action='list.php' method='get'>
				Specie: <select name='org'>
					<option value='hg19'>Human (hg19)</option>
					<option value='galGal3'>Chicken (galGal3)</option>
					<option value='fr2'>Fugu (fr2)</option>
					<option value='oryLat2'>Medaka (oryLat2)</option>
					<option value='gasAcu1'>Stickleback (gasAcu1)</option>
					<option value='tetNig2'>Tetraodon (tetNig2)</option>
					<option value='danRer7'>Zebrafish (danRer7)</option>
				</select><br/>
				<input type='hidden' name='data' value='cluster'>
				<input type='hidden' name='view' value='region'>
				Chromosome: <input type='text' value='chr1' onfocus=\"if(this.value==this.defaultValue) this.value='';\" name='chr' size='10' maxlength='1024'/>
				Region from <input type='text' value='10000000' onfocus=\"if(this.value==this.defaultValue) this.value='';\" name='start' size='10' maxlength='1024'/>
				to <input type='text' value='40000000' onfocus=\"if(this.value==this.defaultValue) this.value='';\" name='stop' size='10' maxlength='1024'/>
        		<input type='submit' value='Search' class='ucnesubmit'/><br/>
			</form>
		</td>
      </tr>
      <tr>
      	<td class='type'>Containing a gene</td>
        <td class='links'>
        	<input type='hidden' name='data' value='cluster'>
        	<form action='view.php' method='get'>
        		<input type='hidden' name='data' value='cluster'>
        		<input type='text' value='Type a gene name ...' onfocus=\"if(this.value==this.defaultValue) this.value='';\" name='entry' size='25' maxlength='1024'/>
        		<input type='submit' value='Search' class='ucnesubmit'/><br/>
        	</form>
        </td>
      </tr>
      <tr>
      	<td class='type'>Containing a UCNE</td>
        <td class='links'>
        	<form action='view.php' method='get'>
        		<input type='hidden' name='data' value='cluster'>
        		<input type='hidden' name='view' value='by_ucne'>
        		<input type='text' value='Type a UCNE id or name...' onfocus=\"if(this.value==this.defaultValue) this.value='';\" name='entry' size='25' maxlength='1024'/>
        		<input type='submit' value='Search' class='ucnesubmit'/><br/>
        	</form>
        </td>
        <td width=10%></td>
      </tr>
    </tbody>
  </table> ";
  } else if ($searchTerm=="gene"){
   	echo "<table class='tblSearch' >
    <tbody>
      <th><td style='padding: 0px;'></td><td style='padding: 0px;'></td><td width=10% style='padding: 0px;'></td></th>
      <tr>
      	<td class='type'>Located within a cluster</td>
        <td class='links'>
        	<form action='list.php' method='get'>
        		<input type='hidden' name='data' value='gene'>
        		<input type='hidden' name='view' value='by_cluster'>
        		<input type='text' value='Type a cluster name or id...' onfocus=\"if(this.value==this.defaultValue) this.value='';\" name='entry' size='30' maxlength='1024'/>
        		<input type='submit' value='Search' class='ucnesubmit'/><br/>
        	</form>
        </td>
      </tr>
      <tr>
      	<td class='type'>In proximity of a UCNE</td>
        <td class='links'>
        	<form action='list.php' method='get'>
        		<input type='hidden' name='data' value='gene'>
        		<input type='hidden' name='view' value='by_ucne'>
        		Max distance <input type='text' value='1000000' onfocus=\"if(this.value==this.defaultValue) this.value='';\" name='flank' size='10' maxlength='1024'/> bps <br/>
        		of the UCNE: <input type='text' value='Type a UCNE name or id...' onfocus=\"if(this.value==this.defaultValue) this.value='';\" name='entry' size='25' maxlength='1024'/>
        		<input type='submit' value='Search' class='ucnesubmit'/><br/>
        	</form>
        </td>
      </tr>
    </tbody>
  </table> ";
  }
?>