function getSpecies(str, data)
{
if (str=="")
  {
  document.getElementById("chrList").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("chrList").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","species_chr.php?data="+data+"&species="+str,true);
xmlhttp.send();
}

function getRegion(data, gene)
{
if (data=="")
  {
  document.getElementById("geneRegionData").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("geneRegionData").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","gene_region.php?region="+data+"&value="+gene,true);
xmlhttp.send();
}

function getSearchTerm(data)
{
if (data=="")
  {
  document.getElementById("advSearchOptions").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("advSearchOptions").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","search_option.php?searchTerm="+data,true);
xmlhttp.send();
}


function toggle(i) {
	var ele = document.getElementById("toggleText"+i);
	var text = document.getElementById("displayText"+i);
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "show";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "hide";
	}
}

function toggleParalogs(i) {
	var ele = document.getElementById("toggleParalogsText"+i);
	var imgE = document.getElementById("displayImg"+i);
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		imgE.innerHTML = "<img src='./Icons/plus.gif'>";
  	}
	else {
		ele.style.display = "block";
		imgE.innerHTML = "<img src='./Icons/minus.gif'>";
	}
}

function toggleGenes() {
	var ele = document.getElementById("toggleGenesText");
	var text = document.getElementById("displayGenesText");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "<img src='./Icons/plus.gif'> <small>show all</small>";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "<img src='./Icons/minus.gif'> <small>show only the top 25</small>";
	}
}

function toggleGenes2() {
	var ele = document.getElementById("toggleGenesText2");
	var text = document.getElementById("displayGenesText2");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "<img src='./Icons/show_img.png'> <small>View cluster image</small>";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "<img src='./Icons/hide_img.png'> <small>Hide cluster image</small>";
	}
}

