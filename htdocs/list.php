<?php readfile("header.php"); ?>
<?php require_once('config.inc.php'); ?>


<div style='font-size: 12px; text-align: justify;'>
<p>
		<?php
			$con = mysqli_connect($config['database']['host'], $config['database']['user'], $config['database']['password']);
			if (!$con)	{die('Could not connect: ' . mysqli_connect_error());}
			mysqli_select_db($con, "UGRB");
			$species=$_GET["org"];
			if ($species=="zebrafish") $assembly="danRer7";
				else if ($species=="fugu") $assembly="fr2";
				else if ($species=="tetraodon") $assembly="tetNig2";
				else if ($species=="stickleback") $assembly="gasAcu1";
				else if ($species=="medaka") $assembly="oryLat2";
				else if ($species=="ciona_intestinalis") $assembly="ci2";
				else if ($species=="lancelet") $assembly="braFlo1";
				else if ($species=="lamprey") $assembly="petMar1";
				else if ($species=="armadillo") $assembly="dasNov1";
				else if ($species=="zebra_finch") $assembly="taeGut1";
				else if ($species=="sea_urchin") $assembly="strPur2";
				else if ($species=="painted_turtle") $assembly="chrPic1";
				else if ($species=="mouse") $assembly="mm10";
				else if ($species=="platypus") $assembly="ornAna1";
				else if ($species=="opossum") $assembly="monDom5";
				else if ($species=="xenopus") $assembly="xenTro3";
				else if ($species=="lizard") $assembly="anoCar2";
				else if ($species=="chicken") $assembly="gg3";
				else if ($species=="hg19") $assembly="hg19";
				else if ($species=="gg3") $assembly="gg3";
			if ($_GET["data"]=="ucne"){
				if ($_GET["view"]=="chr"){
					$value=$_GET["value"];
					if ($species=="hg19"){
						echo "<p style='font-size: 13px; color: #000000;'>Below is a list of <b><span style='color:#b71119;'>Ultraconserved Non-Coding Elements (UCNEs)</span></b> that reside on the chromosome ".$value." in the Human genome (hg19 assembly).</p>
						<div align='center'>
						<table class='ucnelist'>
							<tr><th width='70'>UCNE name</th><th width='50'>UCNE ID</th><th width='170'>Position</th><th width='60'>Type</th><th width='50'>Length</th>
							<th width='60'>Overlapping gene</th><th width='60'>Upstream gene</th><th width='60'>Downstream gene</th><th >Paralogs</th></tr>";
						$sql="SELECT * FROM ucne_hg19_details_coord c, ucne_names n where chr='".$value."' and c.id=n.id order by chr, start ";
						$result = mysqli_query($con, $sql);
						while($row = mysqli_fetch_array($result)){
  							echo " <tr>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["name"]."</a></td>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["id"]."</a></td>
  								<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["chr"].":".$row["start"]."-".$row["stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed' target='_blank'>".$row["chr"].":".$row["start"]."-".$row["stop"]." <img src='./Icons/ext.gif'/></a></td>
  								<td>".$row["type"]."</td>
  								<td>".$row["len"]."</td>";
  								//find neighb. genes
  								$o_gene="";
  								$up_gene="";
  								$dw_gene="";
  								if ($row["type"]!="intergenic"){
  									$sql2="SELECT gene FROM ucne_overlaping_genes where id=".$row["id"]." ";
									$result2 = mysqli_query($con, $sql2);
									while($row2 = mysqli_fetch_array($result2)){
									 $o_gene=$row2["gene"];
									}
								} else {
									$sql3="SELECT leftgene, rightgene FROM ucne_close_genes where id=".$row["id"]." ";
									$result3 = mysqli_query($con, $sql3);
									while($row3 = mysqli_fetch_array($result3)){
									 $up_gene=$row3["leftgene"];
									 $dw_gene=$row3["rightgene"];
									}
								}
								echo "<td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$o_gene."' target='_blank'>".$o_gene."</a></td><td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$up_gene."' target='_blank'>".$up_gene."</a></td><td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$dw_gene."' target='_blank'>".$dw_gene."</a></td>";

								//find paralogs
								$paralogs="";
								$sql4="SELECT chr, seq_start, seq_stop FROM ucne_paralogs where id=".$row["id"]." and evalue<0.0001";
								$result4 = mysqli_query($con, $sql4);
								while($row4 = mysqli_fetch_array($result4)){
									$paralogs=$paralogs."<a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row4["chr"].":".$row4["seq_start"]."-".$row4["seq_stop"]."' target='_blank'>".$row4["chr"].":".$row4["seq_start"]."-".$row4["seq_stop"]."</a>; ";
								}
								echo "<td>".$paralogs."</td>";
  							echo "</tr>";
  						}
  					}
		 		} // end of "view"=="chr"
		 		else if ($_GET["view"]=="gene"){
					$value=$_GET["value"];
					if ($species=="hg19"){
						echo "<p style='font-size: 13px; color: #000000;'>Below is a list of <b><span style='color:#b71119;'>Ultraconserved Non-Coding Elements (UCNEs)</span></b> that reside
						<div id='regionid'>
         					<form >
         						<p style='margin-left:2cm;'>
    							<input type='radio' name='region' value='intragenic' onclick='getRegion(this.value, \"".$value."\")' checked='yes'/><u><b>within</b></u> the noncoding regions of the <b><span style='color:#b71119;'>gene ".$value."</span></b> (introns, UTRs)<br/>
    							<input type='radio' name='region' value='flanking' onclick='getRegion(this.value,  \"".$value."\")'/><u><b>within and flanking 1Mbp region</b></u> of the <b><span style='color:#b71119;'>gene ".$value."</span></b>
    							<br/>
    							</p>
							</form>
		  				</div>
		  				</p>
						<div align='center'>
						<div id='geneRegionData'>
						<table class='ucnelist'>
							<tr><th width='100'>UCNE name</th><th width='50'>UCNE ID</th><th width='170'>Position</th><th width='60'>Type</th><th width='50'>Length</th>
							<th width='60'>Overlapping gene</th><th>Paralogs</th></tr>";
						$sql="SELECT * FROM ucne_hg19_details_coord c, ucne_names n, ucne_overlaping_genes g where c.id=n.id and c.id=g.id and g.gene='".$value."' order by chr, start ";
						$result = mysqli_query($con, $sql);
						while($row = mysqli_fetch_array($result)){
  							echo " <tr>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["name"]."</a></td>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["id"]."</a></td>
  								<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["chr"].":".$row["start"]."-".$row["stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed'>".$row["chr"].":".$row["start"]."-".$row["stop"]."</a></td>
  								<td>".$row["type"]."</td>
  								<td>".$row["len"]."</td>
  								<td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row["gene"]."' target='_blank'>".$row["gene"]."</a></td>";
								//find paralogs
								$paralogs="";
								$sql4="SELECT chr, seq_start, seq_stop FROM ucne_paralogs where id=".$row["id"]." and evalue<0.0001";
								$result4 = mysqli_query($con, $sql4);
								while($row4 = mysqli_fetch_array($result4)){
									$paralogs=$paralogs."<a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row4["chr"].":".$row4["seq_start"]."-".$row4["seq_stop"]."' target='_blank'>".$row4["chr"].":".$row4["seq_start"]."-".$row4["seq_stop"]."</a>; ";
								}
								echo "<td>".$paralogs."</td>";
  							echo "</tr>";
  						}
  						echo "</table></div></div>";
  						echo "<div>
						<table >";
  					}
		 		} // end of "view"=="gene"
		 		else if (($_GET["view"]=="condorID") or ($_GET["view"]=="vistaID") or ($_GET["view"]=="uceID")) {
					$value=$_GET["value"];
					if ($species=="hg19"){


						if ($_GET["view"]=="condorID"){
							$sql="SELECT * FROM condor_ucne_map m, ucne_hg19_details_coord c, ucne_names n, ucne_overlaping_genes g where m.condor_id='".$value."' and m.ucne_id=c.id and c.id=n.id and c.id=g.id order by chr, start ";
							$db="<a href='http://condor.nimr.mrc.ac.uk/index.html' target='_blank'>CONDOR database</a>";
						}else if ($_GET["view"]=="vistaID") {
							$sql="SELECT * FROM vista_ucne_map m, ucne_hg19_details_coord c, ucne_names n, ucne_overlaping_genes g where (m.vista_id='".$value."' or replace(m.vista_id,'element_','hs')='".$value."') and m.ucne_id=c.id and c.id=n.id and c.id=g.id order by chr, start ";
							$db="<a href='http://enhancer.lbl.gov/frnt_page_n.shtml' target='_blank'>VISTA Enhancer Browser</a>";
						}else {
							$sql="SELECT * FROM uce_ucne_map m, ucne_hg19_details_coord c, ucne_names n, ucne_overlaping_genes g where m.uce_id='".$value."' and m.ucne_id=c.id and c.id=n.id and c.id=g.id order by chr, start ";
							$db="<a href='http://users.soe.ucsc.edu/~jill/ultra.html' target='_blank'>UCE (Bejerano et al. 2004) collection</a>";
						}
						echo "<br/><p style='font-size: 13px; color: #000000;'>Below is a list of <b><span style='color:#b71119;'>Ultraconserved Non-Coding Elements (UCNEs)</span></b> that overlap with a non-coding element
						with ID = <i>".$value."</i> from the ".$db."</p>";


						$ucne_rows="";
						$result = mysqli_query($con, $sql);
						$r=0;
						while($row = mysqli_fetch_array($result)){
							$r++;
  							$ucne_rows=$ucne_rows." <tr>
  								<td style='padding: 5px;'><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["name"]."</a></td>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["id"]."</a></td>
  								<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["chr"].":".$row["start"]."-".$row["stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed'>".$row["chr"].":".$row["start"]."-".$row["stop"]."</a></td>
  								<td>".$row["type"]."</td>
  								<td>".$row["len"]."</td>
  								<td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row["gene"]."' target='_blank'>".$row["gene"]."</a></td></tr>";
  						}
  						if ($r>0) {
  							echo "<table class='ucnelist'>
							<tr><th width='100' style='padding: 5px;'>UCNE name</th><th width='50'>UCNE ID</th><th width='170'>Position</th><th width='60'>Type</th><th width='50'>Length</th>
							<th width='160'>Overlapping gene</th></tr>";
							echo $ucne_rows;

  						} else {
  							echo "<br/><p><b>Sorry... Your search did not return any result. Check if you have selected the right database or try with another ID.</b></p>";
  						}
  						echo "</table>";
  						echo "<div>
						<table >";
  					}
		 		} // end of "view"=="external ID"
		 		else if ($_GET["view"]=="flank_gene"){
					$gene=$_GET["gene"];
					$flank=$_GET["flank"];
					if ($species=="hg19"){
						echo "<p style='font-size: 13px; color: #000000;'>Below is a list of <b><span style='color:#b71119;'>Ultraconserved Non-Coding Elements (UCNEs)</span></b> that reside within and flanking ".$flank." bps region</b></u> of the <b><span style='color:#b71119;'>gene ".$gene."</span>.</b>
		  				</p>
						<div align='center'>
						<div id='geneRegionData'>
						<table class='ucnelist'>
							<tr><th width='100'>UCNE name</th><th width='50'>UCNE ID</th><th width='170'>Position</th><th width='60'>Type</th><th width='50'>Length</th>
							<th width='60'>Overlapping gene</th><th>Paralogs</th></tr>";
						$sql="SELECT u.*, name FROM ucne_hg19_details_coord u, refgene_hs19_v2 r, ucne_names n where r.gene='".$gene."' and r.chrom=u.chr and u.start>(r.txStart-".$flank.") and u.stop<(r.txEnd+".$flank.") and u.id=n.id order by chr, start";
						$result = mysqli_query($con, $sql);
						while($row = mysqli_fetch_array($result)){
  							echo " <tr>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["name"]."</a></td>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["id"]."</a></td>
  								<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["chr"].":".$row["start"]."-".$row["stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed'>".$row["chr"].":".$row["start"]."-".$row["stop"]."</a></td>
  								<td>".$row["type"]."</td>
  								<td>".$row["len"]."</td>
  								<td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row["gene"]."' target='_blank'>".$row["gene"]."</a></td>";
								//find paralogs
								$paralogs="";
								$sql4="SELECT chr, seq_start, seq_stop FROM ucne_paralogs where id=".$row["id"]." and evalue<0.0001";
								$result4 = mysqli_query($con, $sql4);
								while($row4 = mysqli_fetch_array($result4)){
									$paralogs=$paralogs."<a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row4["chr"].":".$row4["seq_start"]."-".$row4["seq_stop"]."' target='_blank'>".$row4["chr"].":".$row4["seq_start"]."-".$row4["seq_stop"]."</a>; ";
								}
								echo "<td>".$paralogs."</td>";
  							echo "</tr>";
  						}
  						echo "</table></div></div>";
  						echo "<div>
						<table >";
  					}
		 		} // end of "view"=="flank_gene"
		 		else if ($_GET["view"]=="region"){
		 			$chr=$_GET["chr"];
		 			$start=$_GET["start"];
		 			$stop=$_GET["stop"];
					if ($species=="hg19" or $species=="gg3"){
						$sql="SELECT c.id, name, c.chr, c.start, c.stop, replace(replace(dc.type,'UTR3','UTR'),'UTR5','UTR') as type, dc.len FROM ucne_coord c, ucne_hg19_details_coord dc, ucne_names n where c.specie='".$species."' and c.chr='".$chr."' and c.id=dc.id and c.id=n.id and ((c.stop<=".$stop." and c.stop>=".$start.") or (c.start<=".$stop." and c.start>=".$start.")) order by c.chr, c.start";

						echo "<p style='font-size: 13px; color: #000000;'>Below is a list of <b><span style='color:#b71119;'>Ultraconserved Non-Coding Elements (UCNEs)</span></b> that overlap with the region ".$chr.":".$start."-".$stop." in the ".$species." assembly.</p>
						<div style='margin-left:2cm;' >
						<table class='ucnelist'>
						<tr><th width='70'>UCNE name</th><th width='50'>UCNE ID</th><th width='170'>Position</th><th width='60'>Type</th><th width='50'>Length</th>
						<th width='60'>Overlapping gene</th><th width='60'>Upstream gene</th><th width='60'>Downstream gene</th><th>Paralogs</th></tr>";
						$result = mysqli_query($con, $sql);
						while($row = mysqli_fetch_array($result)){
  							echo " <tr>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["name"]."</a></td>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["id"]."</a></td>
  								<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["chr"].":".$row["start"]."-".$row["stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/UCNEs_".$assembly.".bed' target='_blank'>".$row["chr"].":".$row["start"]."-".$row["stop"]."</a></td>
  								<td>".$row["type"]."</td>
  								<td>".$row["len"]."</td>";
  								//find neighb. genes
  								$o_gene="";
  								$up_gene="";
  								$dw_gene="";
  								if ($row["type"]!="intergenic"){
  									$sql2="SELECT gene FROM ucne_overlaping_genes where id=".$row["id"]." ";
									$result2 = mysqli_query($con, $sql2);
									while($row2 = mysqli_fetch_array($result2)){
									 $o_gene=$row2["gene"];
									}
								} else {
									$sql3="SELECT leftgene, rightgene FROM ucne_close_genes where id=".$row["id"]." ";
									$result3 = mysqli_query($con, $sql3);
									while($row3 = mysqli_fetch_array($result3)){
									 $up_gene=$row3["leftgene"];
									 $dw_gene=$row3["rightgene"];
									}
								}
								echo "<td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$o_gene."' target='_blank'>".$o_gene."</a></td><td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$up_gene."' target='_blank'>".$up_gene."</a></td><td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$dw_gene."' target='_blank'>".$dw_gene."</a></td>";

								//find paralogs
								$paralogs="";
								$sql4="SELECT chr, seq_start, seq_stop FROM ucne_paralogs where id=".$row["id"]." and evalue<0.0001 ";
								$result4 = mysqli_query($con, $sql4);
								while($row4 = mysqli_fetch_array($result4)){
									$paralogs=$paralogs."<a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row4["chr"].":".$row4["seq_start"]."-".$row4["seq_stop"]."' target='_blank'>".$row4["chr"].":".$row4["seq_start"]."-".$row4["seq_stop"]."</a>; ";
								}
								echo "<td>".$paralogs."</td>";
  							echo "</tr>";
  						}
  					} else {
						$sql="SELECT c.id, name, c.chr as seq_chr, c.seq_start, c.seq_stop, dc.chr, dc.start, dc.stop FROM whole_genome_aln_hits c, ucne_hg19_details_coord dc, ucne_names n
						where c.specie='".$species."' and c.evalue<0.0001 and c.chr='".$chr."' and c.id=dc.id and c.id=n.id and ((c.seq_stop<=".$stop." and c.seq_stop>=".$start.") or (c.seq_start<=".$stop." and c.seq_start>=".$start."))
						order by c.chr, c.seq_start";
						echo "<p style='font-size: 13px; color: #000000;'>Below is a list of <b><span style='color:#b71119;'>Ultraconserved Non-Coding Elements (UCNEs)</span></b> that overlap with the region ".$chr.":".$start."-".$stop." in ".$species.".</p>
						<div style='margin-left:2cm;'>
						<table class='ucnelist'>
						<tr><th width='100'>UCNE name</th><th width='70'>UCNE ID</th><th width='200'>Position in human (hg19)</th><th width='200'>Position in ".$species."</th></tr>";
						$result = mysqli_query($con, $sql);
						while($row = mysqli_fetch_array($result)){
							echo " <tr>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["name"]."</a></td>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["id"]."</a></td>
  								<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["chr"].":".$row["start"]."-".$row["stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/UCNEs_hg19.bed' target='_blank'>".$row["chr"].":".$row["start"]."-".$row["stop"]."</a></td>
  								<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=".$species."&db=".$assembly."&position=".$row["seq_chr"].":".$row["seq_start"]."-".$row["seq_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/UCNEs_".$assembly.".bed' target='_blank'>".$row["seq_chr"].":".$row["seq_start"]."-".$row["seq_stop"]."</a></td>
  								</tr>";
						}
					}
		 		} // end of "view"=="region"
		 		else if ($_GET["view"]=="paralogs"){
					echo "<p style='font-size: 13px; color: #000000;'>Below is a list of <b><span style='color:#b71119;'>paralogous Ultraconserved Non-Coding Elements (UCNEs)</span></b> in the Human genome (hg19 assembly).</p>
						<div align='center'>
						<table class='ucnelist'><tr><th width='70'>UCNE name</th><th width='50'>UCNE ID</th><th width='180'>UCNE position</th><th width='60'>UCNE type</th>
						<th width='50'>Paralogs</th></tr>";
						$sql="SELECT distinct p.id, c.chr, c.start, c.stop, name, replace(replace(c.type,'UTR3','UTR'),'UTR5','UTR') as type FROM ucne_paralogs p, ucne_hg19_details_coord c, ucne_names n where p.evalue<0.0001 and prss3_evalue<0.0001 and p.id=c.id and p.id=n.id order by name";
						$result = mysqli_query($con, $sql);
						while($row = mysqli_fetch_array($result)){
							echo " <tr>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'><b>".$row["name"]."</b></a></td>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["id"]."</a></td>
  								<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["chr"].":".$row["start"]."-".$row["stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed' target='_blank'>".$row["chr"].":".$row["start"]."-".$row["stop"]."</a></td>
  								<td>".$row["type"]."</td><td style='padding-left: 5px; text-align:left'>";
  								$sqlp="SELECT p.seq_start, p.chr, p.seq_stop, p.identity, p.evalue, p.paralog_ucne FROM ucne_paralogs p where p.id=".$row["id"]." and p.evalue<0.0001";
								$resultp = mysqli_query($con, $sqlp);
								$paralog_reg="";
								$paralog_ucnes="";
								while($rowp = mysqli_fetch_array($resultp)){
									$arr_par_ucne=explode(";", $rowp["paralog_ucne"]);
								 	//echo "<a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$rowp["chr"].":".$rowp["seq_start"]."-".$rowp["seq_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed' target='_blank'>".$rowp["chr"].":".$rowp["seq_start"]."-".$rowp["seq_stop"]."</a>; ";
									if ($rowp["paralog_ucne"]=="") $paralog_reg=$paralog_reg."<a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$rowp["chr"].":".$rowp["seq_start"]."-".$rowp["seq_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/UCNEs_hg19.bed'>".$rowp["chr"].":".$rowp["seq_start"]."-".$rowp["seq_stop"]."</a>; ";
									foreach ($arr_par_ucne as $par_ucne){
  										$par_ucne_name="";
  										if ($par_ucne!=""){
 											$sqln="SELECT name FROM ucne_names where id=".$par_ucne." ";
											$resultn = mysqli_query($con, $sqln);
											if($rown = mysqli_fetch_array($resultn)) $par_ucne_name=$rown["name"];
 											$paralog_ucnes=$paralog_ucnes." <a class='link' href='./view.php?data=ucne&entry=".$par_ucne."'>".$par_ucne_name."</a>; ";
 										}
 									}
 									//$paralogs=$paralogs."<br/>";
								}
							echo "<i>Paralog UCNEs: </i>";
							if ($paralog_ucnes!="") echo $paralog_ucnes."<br/>";
								else echo "/<br/>";
							echo "<i>Other paralog regions: </i>";
							if ($paralog_reg!="") echo $paralog_reg."<br/>";
								else echo "/<br/>";
							echo "</td></tr>";
						}

		 		} // end of "view"=="paralogs"
		 		else if ($_GET["view"]=="bylength"){
		 			$value=$_GET["value"];
					if ($species=="hg19" or $species=="gg3"){
						echo "<p style='font-size: 13px; color: #000000;'>Below is a list of <b><span style='color:#b71119;'>Ultraconserved Non-Coding Elements (UCNEs)</span></b> that are longer  than ".$value." bps. </p>
						<div align='center'>
						<table class='ucnelist'>
							<tr><th width='70'>UCNE name</th><th width='50'>UCNE ID</th><th width='170'>Position</th><th width='60'>Type</th><th width='50'>Length</th>
							<th width='60'>Overlapping gene</th><th width='60'>Upstream gene</th><th width='60'>Downstream gene</th><th >Paralogs</th></tr>";
						$sql="SELECT * FROM ucne_hg19_details_coord c, ucne_names n where len>=".$value." and c.id=n.id order by chr, start";
						$result = mysqli_query($con, $sql);
						while($row = mysqli_fetch_array($result)){
  							echo " <tr>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["name"]."</a></td>
  								<td><a class='link' href='./view.php?data=ucne&entry=".$row["id"]."'>".$row["id"]."</a></td>
  								<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["chr"].":".$row["start"]."-".$row["stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed' target='_blank'>".$row["chr"].":".$row["start"]."-".$row["stop"]."</a></td>
  								<td>".$row["type"]."</td>
  								<td>".$row["len"]."</td>";
  								//find neighb. genes
  								$o_gene="";
  								$up_gene="";
  								$dw_gene="";
  								if ($row["type"]!="intergenic"){
  									$sql2="SELECT gene FROM ucne_overlaping_genes where id=".$row["id"]." ";
									$result2 = mysqli_query($con, $sql2);
									while($row2 = mysqli_fetch_array($result2)){
									 $o_gene=$row2["gene"];
									}
								} else {
									$sql3="SELECT leftgene, rightgene FROM ucne_close_genes where id=".$row["id"]." ";
									$result3 = mysqli_query($con, $sql3);
									while($row3 = mysqli_fetch_array($result3)){
									 $up_gene=$row3["leftgene"];
									 $dw_gene=$row3["rightgene"];
									}
								}
								echo "<td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$o_gene."' target='_blank'>".$o_gene."</a></td>
								<td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$up_gene."' target='_blank'>".$up_gene."</a></td>
								<td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$dw_gene."' target='_blank'>".$dw_gene."</a></td>";

								//find paralogs
								$paralogs="";
								$sql4="SELECT chr, seq_start, seq_stop FROM ucne_paralogs where id=".$row["id"]." and evalue<0.0001";
								$result4 = mysqli_query($con, $sql4);
								while($row4 = mysqli_fetch_array($result4)){
									$paralogs=$paralogs."<a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row4["chr"].":".$row4["seq_start"]."-".$row4["seq_stop"]."' target='_blank'>".$row4["chr"].":".$row4["seq_start"]."-".$row4["seq_stop"]."</a>; ";
								}
								echo "<td>".$paralogs."</td>";
  							echo "</tr>";
  						}
  					}
		 		} // end of "view"=="bylength"
		////////////////////////////////////////////////////////////////////////////////////////////////////////////// clusters
		 	} else if ($_GET["data"]=="cluster"){ // clusters
		 		if ($_GET["view"]=="chr"){
		 			$value=$_GET["value"];
		 			if ($species=="hg19"){
		 				echo "<p style='font-size: 13px; color: #000000;'>Below is a list of <b><span style='color:#b71119;'>Ultraconserved Genomic Regulatory Blocks (UGRBs)</span></b> that reside on the chromosome ".$value." in the Human genome (hg19 assembly).</p>
						<div align='center'>
						<table class='ucnelist'><tr><th width='70'>Cluster name</th><th width='50'>Cluster ID</th><th width='170'>Position</th><th width='50'>#UCNEs</th><th width='200'>Associated genes</th>
							<th >UCNEs forming the cluster</th></tr>";
						$sql="SELECT c.cluster_id, hs_chr, hs_start, hs_stop, name FROM clusters c, clusters_repr_names n where hs_chr='".$value."' and c.cluster_id=n.cluster_id order by hs_chr, hs_start ";
						$result = mysqli_query($con, $sql);
						$i=-1;
						while($row = mysqli_fetch_array($result)){
							$i++;
  							$sql3="SELECT count(*) as c FROM ucne_to_clusters where cluster_id=".$row["cluster_id"]." ";
							$result3 = mysqli_query($con, $sql3);
							while($row3 = mysqli_fetch_array($result3)){
								if ($row3["c"]>2){
									echo " <tr>
									<td><a class='link' href='./view.php?data=cluster&entry=".$row["cluster_id"]."'>".$row["name"]."_cluster</a></td>
  									<td><a class='link' href='./view.php?data=cluster&entry=".$row["cluster_id"]."'>".$row["cluster_id"]."</a></td>
  									<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed' target='_blank'>".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]."</a></td>
  									";
  									echo "<td>".$row3["c"]."</td>";
  									$genes="";
  									$sql2="select gene from cluster_genes where cluster_id=".$row["cluster_id"]." and specie='".$species."' ";
									$result2 = mysqli_query($con, $sql2);
									while($row2 = mysqli_fetch_array($result2)){
										$genes=$genes."<a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row2["gene"]."' target='_blank'>".$row2["gene"]."</a>; ";
									}
									echo "<td style='padding-left: 5px; text-align:left'>".$genes."</td>";

									$sql4="SELECT u.ucne_id, name FROM ucne_to_clusters u, ucne_names n where u.ucne_id=n.id and cluster_id=".$row["cluster_id"]." ";
									$result4 = mysqli_query($con, $sql4);
									$ucnes="";
									while($row4 = mysqli_fetch_array($result4)){
										$ucnes=$ucnes."<a class='link' href='./view.php?data=ucne&entry=".$row4["ucne_id"]."'>".$row4["name"]."</a>; ";
									}
									echo "<td style='padding-left: 5px; text-align:left'>
										<a id='displayText".$i."' href='javascript:toggle(".$i.");' class='link'>show</a>
										<div id='toggleText".$i."' style='display: none'>".$ucnes."</div></td>";
									echo "</tr>";
								}
							}
						}
		 			}
		 		} // end of "view"=="chr"
		 		else if ($_GET["view"]=="region"){
		 			$chr=$_GET["chr"];
		 			$start=$_GET["start"];
		 			$stop=$_GET["stop"];
		 			if ($species=="hg19" or $species=="gg3" ){
		 				echo "<p style='font-size: 13px; color: #000000;'>Below is a list of <b><span style='color:#b71119;'>Ultraconserved Genomic Regulatory Blocks (UGRBs)</span></b> that reside on the chromosome ".$value." in the Human genome (hg19 assembly).</p>
						<div align='center'>
						<table class='ucnelist'><tr><th width='70'>Cluster name</th><th width='50'>Cluster ID</th><th width='200'>Position</th><th width='50'>#UCNEs</th><th width='200'>Associated genes</th>
							<th>UCNEs forming the cluster</th></tr>";
						$sql="SELECT c.cluster_id, hs_chr, hs_start, hs_stop, name FROM clusters c, clusters_repr_names n where hs_chr='".$chr."' and ((hs_stop<=".$stop." and hs_stop>=".$start.") or (hs_start<=".$stop." and hs_start>=".$start.")) and c.cluster_id=n.cluster_id order by hs_chr, hs_start ";
						$i=-1;
						$result = mysqli_query($con, $sql);
						while($row = mysqli_fetch_array($result)){
							$i++;
  							$sql3="SELECT count(*) as c FROM ucne_to_clusters where cluster_id=".$row["cluster_id"]." ";
							$result3 = mysqli_query($con, $sql3);
							while($row3 = mysqli_fetch_array($result3)){
								if ($row3["c"]>2){
									echo " <tr>
									<td><a class='link' href='./view.php?data=cluster&entry=".$row["cluster_id"]."'>".$row["name"]."_cluster</a></td>
  									<td><a class='link' href='./view.php?data=cluster&entry=".$row["cluster_id"]."'>".$row["cluster_id"]."</a></td>
  									<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed' target='_blank'>".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]."</a></td>
  									";
  									echo "<td>".$row3["c"]."</td>";
  									$genes="";
  									$sql2="select gene from cluster_genes where cluster_id=".$row["cluster_id"]." and specie='".$species."' ";
									$result2 = mysqli_query($con, $sql2);
									while($row2 = mysqli_fetch_array($result2)){
										$genes=$genes."<a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row2["gene"]."' target='_blank'>".$row2["gene"]."</a>; ";
									}
									echo "<td style='padding-left: 5px; text-align:left'>".$genes."</td>";
									$sql4="SELECT u.ucne_id, name FROM ucne_to_clusters u, ucne_names n where u.ucne_id=n.id and cluster_id=".$row["cluster_id"]." ";
									$result4 = mysqli_query($con, $sql4);
									$ucnes="";
									while($row4 = mysqli_fetch_array($result4)){
										$ucnes=$ucnes."<a class='link' href='./view.php?data=ucne&entry=".$row4["ucne_id"]."'>".$row4["name"]."</a>; ";
									}
									echo "<td style='padding-left: 5px; text-align:left'>
										<a id='displayText".$i."' href='javascript:toggle(".$i.");' class='link'>show</a>
										<div id='toggleText".$i."' style='display: none'>".$ucnes."</div></td>";
									echo "</tr>";
								}
							}
						}
		 			}
		 		} // end of "view"=="chr"
		 		elseif ($_GET["view"]=="top25") {
		 			echo "<p style='font-size: 13px; color: #000000;'>Below is a list of the top 25 <b><span style='color:#b71119;'>Ultraconserved Genomic Regulatory Blocks (UGRBs)</span></b> ranked by the number of Ultraconserved Non-Coding Elements (UCNEs) forming the regulatory block in the Human genome (hg19 assembly).</p><br/>
						<div align='center'>
						<table class='ucnelist'>
						<tr><th width='70'>Cluster name</th><th width='50'>Cluster ID</th><th width='200'>Position</th><th width='50'>#UCNEs</th><th width='200'>Associated genes</th>
						<th>UCNEs forming the cluster</th></tr>";
					$sql="SELECT cl.cluster_id, hs_chr, hs_start, hs_stop, cnt, name FROM clusters cl, (SELECT cluster_id, count(*) as cnt FROM ucne_to_clusters u group by cluster_id order by cnt desc limit 0, 25) uc, clusters_repr_names n
						where cl.cluster_id=uc.cluster_id and cl.cluster_id=n.cluster_id order by uc.cnt desc ";
					$result = mysqli_query($con, $sql);
					$i=-1;
					while($row = mysqli_fetch_array($result)){
						$i++;
						echo " <tr>
						<td><a class='link' href='./view.php?data=cluster&entry=".$row["cluster_id"]."'>".$row["name"]."_cluster</a></td>
  						<td><a class='link' href='./view.php?data=cluster&entry=".$row["cluster_id"]."'>".$row["cluster_id"]."</a></td>
  						<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed' target='_blank'>".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]."</a></td>
  						";
  						echo "<td>".$row["cnt"]."</td>";
  						$genes="";
  						$sql2="select gene from cluster_genes where cluster_id=".$row["cluster_id"]." and specie='".$species."' ";
						$result2 = mysqli_query($con, $sql2);
						while($row2 = mysqli_fetch_array($result2)){
							$genes=$genes."<a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row2["gene"]."' target='_blank'>".$row2["gene"]."</a>; ";
						}
						echo "<td style='padding-left: 5px; text-align:left'>".$genes."</td>";
						$sql4="SELECT u.ucne_id, name FROM ucne_to_clusters u, ucne_names n where u.ucne_id=n.id and cluster_id=".$row["cluster_id"]." ";
						$result4 = mysqli_query($con, $sql4);
						$ucnes="";
						while($row4 = mysqli_fetch_array($result4)){
							$ucnes=$ucnes."<a class='link' href='./view.php?data=ucne&entry=".$row4["ucne_id"]."'>".$row4["name"]."</a>; ";
						}
						echo "<td style='padding-left: 5px; text-align:left'>
							<a id='displayText".$i."' href='javascript:toggle(".$i.");' class='link'>show</a>
							<div id='toggleText".$i."' style='display: none'>".$ucnes."</div></td>";
						echo "</tr>";

					}
				}elseif (($_GET["view"]=="allBySize") or ($_GET["view"]=="allByName"))  {
		 			echo "<p style='font-size: 13px; color: #000000;'>Below is a list of all <b><span style='color:#b71119;'>Ultraconserved Genomic Regulatory Blocks (UGRBs)</span></b> ranked by the number of Ultraconserved Non-Coding Elements (UCNEs) forming the regulatory block in the Human genome (hg19 assembly).</p><br/>
						<div align='center'>
						<table class='ucnelist'>
						<tr><th></th><th width='70'>Cluster name</th><th width='50'>Cluster ID</th><th width='200'>Position</th><th width='50'>#UCNEs</th><th width='200'>Associated genes</th>
						<th>UCNEs forming the cluster</th></tr>";
					if ($_GET["view"]=="allBySize") {
						$sql="SELECT cl.cluster_id, hs_chr, hs_start, hs_stop, cnt, name FROM clusters cl, (SELECT cluster_id, count(*) as cnt FROM ucne_to_clusters u group by cluster_id order by cnt desc) uc, clusters_repr_names n
							where cl.cluster_id=uc.cluster_id and cl.cluster_id=n.cluster_id order by uc.cnt desc ";
					} else {
					$sql="SELECT cl.cluster_id, hs_chr, hs_start, hs_stop, cnt, name FROM clusters cl, (SELECT cluster_id, count(*) as cnt FROM ucne_to_clusters u group by cluster_id order by cnt desc) uc, clusters_repr_names n
							where cl.cluster_id=uc.cluster_id and cl.cluster_id=n.cluster_id order by name ";
					}
					$result = mysqli_query($con, $sql);
					$i=-1;
					while($row = mysqli_fetch_array($result)){
						$i++;
						echo " <tr>
						<td>&nbsp".($i+1)."&nbsp</td>
						<td><a class='link' href='./view.php?data=cluster&entry=".$row["cluster_id"]."'>".$row["name"]."_cluster</a></td>
  						<td><a class='link' href='./view.php?data=cluster&entry=".$row["cluster_id"]."'>".$row["cluster_id"]."</a></td>
  						<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed' target='_blank'>".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]."</a></td>
  						";
  						echo "<td>".$row["cnt"]."</td>";
  						$genes="";
  						$sql2="select gene from cluster_genes where cluster_id=".$row["cluster_id"]." and specie='".$species."' ";
						$result2 = mysqli_query($con, $sql2);
						while($row2 = mysqli_fetch_array($result2)){
							$genes=$genes."<a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row2["gene"]."' target='_blank'>".$row2["gene"]."</a>; ";
						}
						echo "<td style='padding-left: 5px; text-align:left'>".$genes."</td>";
						$sql4="SELECT u.ucne_id, name FROM ucne_to_clusters u, ucne_names n where u.ucne_id=n.id and cluster_id=".$row["cluster_id"]." ";
						$result4 = mysqli_query($con, $sql4);
						$ucnes="";
						while($row4 = mysqli_fetch_array($result4)){
							$ucnes=$ucnes."<a class='link' href='./view.php?data=ucne&entry=".$row4["ucne_id"]."'>".$row4["name"]."</a>; ";
						}
						echo "<td style='padding-left: 5px; text-align:left'>
							<a id='displayText".$i."' href='javascript:toggle(".$i.");' class='link'>show</a>
							<div id='toggleText".$i."' style='display: none'>".$ucnes."</div></td>";
						echo "</tr>";

					}
		 		}elseif ($_GET["view"]=="atLeast20") {
		 			echo "<p style='font-size: 13px; color: #000000;'>Below is a list of <b><span style='color:#b71119;'>Ultraconserved Genomic Regulatory Blocks (UGRBs)</span></b> that are formed by at least 20 Ultraconserved Non-Coding Elements (UCNEs) ranked by the number of UCNEs forming the regulatory block in the Human genome (hg19 assembly).</p>
						<div align='center'>
						<table class='ucnelist'><tr><th width='70'>Cluster name</th><th width='50'>Cluster ID</th><th width='200'>Position</th><th width='50'>#UCNEs</th><th width='200'>Associated genes</th>
						<th>UCNEs forming the cluster</th></tr>";
					$i=-1;
					$sql="SELECT cl.cluster_id, hs_chr, hs_start, hs_stop, cnt, name FROM clusters cl, (SELECT cluster_id, count(*) as cnt FROM ucne_to_clusters u group by cluster_id order by cnt) uc, clusters_repr_names n
						where cl.cluster_id=uc.cluster_id and cl.cluster_id=n.cluster_id and cnt>=20 order by cnt desc ";
					$result = mysqli_query($con, $sql);
					while($row = mysqli_fetch_array($result)){
						$i++;
						echo " <tr>
						<td><a class='link' href='./view.php?data=cluster&entry=".$row["cluster_id"]."'>".$row["name"]."_cluster</a></td>
  						<td><a class='link' href='./view.php?data=cluster&entry=".$row["cluster_id"]."'>".$row["cluster_id"]."</a></td>
  						<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed' target='_blank'>".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]."</a></td>
  						";
  						echo "<td>".$row["cnt"]."</td>";
  						$genes="";
  						$sql2="select gene from cluster_genes where cluster_id=".$row["cluster_id"]." and specie='".$species."' ";
						$result2 = mysqli_query($con, $sql2);
						while($row2 = mysqli_fetch_array($result2)){
							$genes=$genes."<a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row2["gene"]."' target='_blank'>".$row2["gene"]."</a>; ";
						}
						echo "<td style='padding-left: 5px; text-align:left'>".$genes."</td>";
						$sql4="SELECT u.ucne_id, name FROM ucne_to_clusters u, ucne_names n where u.ucne_id=n.id and cluster_id=".$row["cluster_id"]." ";
						$result4 = mysqli_query($con, $sql4);
						$ucnes="";
						while($row4 = mysqli_fetch_array($result4)){
							$ucnes=$ucnes."<a class='link' href='./view.php?data=ucne&entry=".$row4["ucne_id"]."'>".$row4["name"]."</a>; ";
						}
						echo "<td style='padding-left: 5px; text-align:left'>
							<a id='displayText".$i."' href='javascript:toggle(".$i.");' class='link'>show</a>
							<div id='toggleText".$i."' style='display: none'>".$ucnes."</div></td>";
						echo "</tr>";

					}
		 		}elseif ($_GET["view"]=="paralogs") {
		 			echo "<p style='font-size: 13px; color: #000000;'>Below is a list of <b><span style='color:#b71119;'>paralogous Ultraconserved Genomic Regulatory Blocks (UGRBs)</span></b> in the Human genome (hg19 assembly).</p>
						<div align='center'>
						<table class='ucnelist'><tr><th width='50'>Paralog Group</th><th width='70'>Cluster name</th><th width='50'>Cluster ID</th><th width='180'>Position</th><th width='50'>#UCNEs</th><th width='200'>Associated genes</th>
						<th>UCNEs forming the cluster</th></tr>";
					$i=-1;
					$sql_pargr="SELECT distinct group_id FROM cluster_paralog_groups";
					$result_pargr = mysqli_query($con, $sql_pargr);
					while($row_pargr = mysqli_fetch_array($result_pargr)){
						$row_gr=0;
						$sql="SELECT cl.cluster_id, hs_chr, hs_start, hs_stop, cnt, name FROM clusters cl, (SELECT cluster_id, count(*) as cnt FROM ucne_to_clusters u group by cluster_id order by cnt) uc, clusters_repr_names n, cluster_paralog_groups p
						where cl.cluster_id=uc.cluster_id and cl.cluster_id=n.cluster_id and cl.cluster_id=p.cluster_id and p.group_id=".$row_pargr["group_id"]." order by name ";
						$result = mysqli_query($con, $sql);
						while($row = mysqli_fetch_array($result)){
							$i++;
							$row_gr++;
							echo " <tr>";
							if ($row_gr==1) echo "<td><b>".$row_pargr["group_id"]."</b></td>";
							else echo "<td></td>";
							echo "<td><a class='link' href='./view.php?data=cluster&entry=".$row["cluster_id"]."'><b>".$row["name"]."_cluster</b></a></td>
  							<td><a class='link' href='./view.php?data=cluster&entry=".$row["cluster_id"]."'>".$row["cluster_id"]."</a></td>
  							<td><a href='http://genome.ucsc.edu/cgi-bin/hgTracks?&clade=mammal&org=Human&db=hg19&position=".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]."&hgt.customText=https://epd.expasy.org/ucnebase/data/ucne_pub/UCNE_hg19_coordinates.bed' target='_blank'>".$row["hs_chr"].":".$row["hs_start"]."-".$row["hs_stop"]."</a></td>
  							";
  							echo "<td>".$row["cnt"]."</td>";
  							$genes="";
  							$sql2="select gene from cluster_genes where cluster_id=".$row["cluster_id"]." and specie='".$species."' ";
							$result2 = mysqli_query($con, $sql2);
							while($row2 = mysqli_fetch_array($result2)){
								$genes=$genes."<a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row2["gene"]."' target='_blank'>".$row2["gene"]."</a>; ";
							}
							echo "<td style='padding-left: 5px; text-align:left'>".$genes."</td>";
							$sql4="SELECT u.ucne_id, name FROM ucne_to_clusters u, ucne_names n where u.ucne_id=n.id and cluster_id=".$row["cluster_id"]." ";
							$result4 = mysqli_query($con, $sql4);
							$ucnes="";
							while($row4 = mysqli_fetch_array($result4)){
								$ucnes=$ucnes."<a class='link' href='./view.php?data=ucne&entry=".$row4["ucne_id"]."'>".$row4["name"]."</a>; ";
							}
							echo "<td style='padding-left: 5px; text-align:left'>
								<a id='displayText".$i."' href='javascript:toggle(".$i.");' class='link'>show</a>
								<div id='toggleText".$i."' style='display: none'>".$ucnes."</div></td>";
							echo "</tr>";

						}
						echo "<tr style=\"background-color:#DBDBDB;\"><td colspan='7'>&nbsp</td></tr>";
					}
		 		} //end paralogs
		 		elseif ($_GET["view"]=="species_summary") {
		 			$species=array("Human","Mouse", "Opossum", "Platypus", "Chicken", "Zebra_finch", "Lizard", "Painted_turtle",  "Xenopus", "Fugu","Medaka","Stickleback","Tetraodon","Zebrafish");

		 			echo "<p style='font-size: 13px; color: #000000;'>Below is a summary of the <b><span style='color:#b71119;'>number of UCNEs found in different species </span></b> forming a corresponding genomic regulatory block.</p>
						<div align='center'>
						<table class='ucnelist'><tr><th width='100'>Cluster name</th>";
					for($i=0;$i<count($species);$i++){
						echo "<th><img src='./images_species/thumb_".$species[$i].".png' align='middle' />".$species[$i]."</th>";
					}
					echo "</tr>";
					$i=-1;

					$sqlc="SELECT c.* FROM clusters_repr_names c,  tmp_cluster_species_summary s where c.cluster_id=s.cluster_id and s.specie='human' order by s.ucne_cnt desc";
					$resultc = mysqli_query($con, $sqlc);
					while($rowc = mysqli_fetch_array($resultc)){
						$ucnes_cnt=array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
					//	$bitscores=array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
						$sql="SELECT * FROM tmp_cluster_species_summary t where cluster_id=".$rowc["cluster_id"];
						$result = mysqli_query($con, $sql);
						while($row = mysqli_fetch_array($result)){
							for($i=0;$i<count($species);$i++){
								if ($row["specie"]==strtolower($species[$i])) {
									$ucnes_cnt[$i]=$row["ucne_cnt"];
								//	$bitscores[$i]=$row["avg_bitscore"];
								}
							//	if (($i==0) or ($i==4)) $bitscores[$i]="n/a";
							}
						}
						echo "<tr><td><a class='link' href='./view.php?data=cluster&entry=".$rowc["cluster_id"]."'><b>".$rowc["name"]."_cluster</b></a></td>";
						for($i=0;$i<count($species);$i++){
							echo "<td><a class='link' href='./view.php?data=cluster&entry=".$rowc["cluster_id"]."#".$species[$i]."'>".$ucnes_cnt[$i]."</a></td>";
							//echo "<td><FONT COLOR='#C00000 '>".$ucnes_cnt[$i]."</font> /<br/>".$bitscores[$i]."</td>";
						}
					//	$sql="SELECT round(avg(avg_bitscore)) as avg_b FROM tmp_cluster_species_summary_2 t where cluster_id=".$rowc["cluster_id"]." and specie<>'human' and specie<>'chicken'";
					//	$result = mysqli_query($con, $sql);
					//	if($row = mysqli_fetch_array($result)){
					//		echo "<td>".$row["avg_b"]."</td>";
					//	}
						echo "</tr>";
					}
		 		} //end paralogs
		///////////////////////////////////////////////////////////
		 	} else if ($_GET["data"]=="gene"){ // genes
				$value=$_GET["entry"];
				if ($_GET["view"]=="by_cluster"){
					$sql="SELECT r.*, n.cluster_id, name FROM cluster_genes c, clusters_repr_names n, refgene_hs19_v2 r
						where c.cluster_id=n.cluster_id and (c.cluster_id='".$value."' or concat(n.name,'_cluster' )='".$value."') and  r.gene=c.gene ";
					echo "<p style='font-size: 13px; color: #000000;'>Below is a list of <b><span style='color:#b71119;'>genes</span></b> that reside within the cluster ".$value." in the Human genome (hg19 assembly).</p>
						<div align='center'>
						<table class='ucnelist'>
						<tr><th width='100'>Gene</th><th width='20'>Strand</th><th width='50'>Chromosome</th><th width='150'>Transcription start</th><th width='150'>Transcription end</th>
						<th width='70'>Cluster id</th><th width='150'>Cluster name</th></tr>";
					$result = mysqli_query($con, $sql);
					while($row = mysqli_fetch_array($result)){
						echo "<tr>
						<td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row["gene"]."' target='_blank'>".$row["gene"]."</td>
  						<td>".$row["strand"]."</td>
  						<td>".$row["chrom"]."</td>
  						<td>".$row["txStart"]."</td>
  						<td>".$row["txEnd"]."</td>
  						<td><a class='link' href='./view.php?data=cluster&entry=".$row["cluster_id"]."'>".$row["cluster_id"]."</a></td>
  						<td><a class='link' href='./view.php?data=cluster&entry=".$row["cluster_id"]."'>".$row["name"]."_cluster</a></td>
  						</tr>";
					}
				}
				else if ($_GET["view"]=="by_ucne") {
					$flank=$_GET["flank"];
					$sql="SELECT r.*, u.id, name FROM ucne_coord u, ucne_names n, refgene_hs19_v2 r
						where u.specie='hg19' and u.id=n.id and (u.id='".$value."' or n.name='') and u.chr=r.chrom and
						(((txStart<=(stop+".$flank.") and txStart>=(start-".$flank.")) or (txEnd<=(stop+".$flank.") and txEnd>=(start-".$flank.")))
						OR (((start-".$flank.")>=txStart and (start-".$flank.")<=txEnd) or ((stop+".$flank.")>=txStart and (stop+".$flank.")<=txEnd)))";
					echo "<p style='font-size: 13px; color: #000000;'>Below is a list of <b><span style='color:#b71119;'>genes</span></b> that reside in a region flanking ".$flank." bps around the UCNE ".$value." in the Human genome (hg19 assembly).</p>
						<div align='center'>
						<table class='ucnelist'>
						<tr><th width='100'>Gene</th><th width='20'>Strand</th><th width='50'>Chromosome</th><th width='150'>Transcription start</th><th width='150'>Transcription end</th>
						<th width='70'>Cluster id</th><th width='150'>Cluster name</th></tr>";
					$result = mysqli_query($con, $sql);
					while($row = mysqli_fetch_array($result)){
						echo "<tr>
						<td><a href='http://www.genecards.org/cgi-bin/carddisp.pl?gene=".$row["gene"]."' target='_blank'>".$row["gene"]."</a></td>
  						<td>".$row["strand"]."</td>
  						<td>".$row["chrom"]."</td>
  						<td>".$row["txStart"]."</td>
  						<td>".$row["txEnd"]."</td>";
  						$sql2="SELECT n.cluster_id, concat(n.name,'_cluster') as name FROM cluster_genes c, clusters_repr_names n where c.specie='hg19' and c.cluster_id=n.cluster_id and c.gene='".$row["gene"]."'";
  						$result2 = mysqli_query($con, $sql2);
						$row2 = mysqli_fetch_array($result2);
  						echo "<td><a class='link' href='./view.php?data=cluster&entry=".$row2["cluster_id"]."'>".$row2["cluster_id"]."</a></td>
  							<td><a class='link' href='./view.php?data=cluster&entry=".$row2["cluster_id"]."'>".$row2["name"]."</a></td>";
  						echo "</tr>";
					}
				}
		 	}
		 	mysqli_close($con);
		 ?>
		</table>
	</div>
</p>
<p>

</p>

</div>




<!-- ######### Insert the footer #########-->
<?php readfile("footer.html"); ?>





</body>
</html>
