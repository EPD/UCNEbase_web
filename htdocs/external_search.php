<?php readfile("header.php"); ?>
<?php require_once('config.inc.php'); ?>


<div style='font-size: 12px; text-align: justify;'>
<p><br/>
<div id='advSearch'>
	<form action='list.php' method='get'>
	<h2>Search for UCNEs by using external accession ID from  <BR/><BR/>
		<div id='advSearch'>
         		<input type='hidden' name='data' value='ucne'>
        		<input type='hidden' name='org' value='hg19'>
    			<input type='radio' name='view' value='condorID' checked='yes'/> <a href='http://condor.nimr.mrc.ac.uk/index.html' target='_blank'>CONDOR database<a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    			<input type='radio' name='view' value='vistaID' /> <a href='http://enhancer.lbl.gov/frnt_page_n.shtml' target='_blank'>VISTA Enhancer Browser</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    			<input type='radio' name='view' value='uceID' /> <a href='http://users.soe.ucsc.edu/~jill/ultra.html' target='_blank'>UCE (Bejerano et al. 2004)</a>
    			<br/>

		</div>
	</h2>
	<p> Type an accession ID of a non-coding element from the corresponding database to see if that non-coding element is present in UCNEbase.<br/>
	<br/><i>Example IDs: <b>CRCNE00002657</b> from CONDOR, <b>element_671</b> or <b>hs671</b> from VISTA, <b>uc.36</b> from UCE (Bejerano et al. 2004) etc.</i><br/>
	</p>
  <table >
    <tbody>
      <tr>
        <td class='links' style='padding-left: 150px;'>
        		<input type='text' value='Type an ID ...' onfocus="if(this.value==this.defaultValue) this.value='';" name='value' size='30' maxlength='1024'/>
        		<input type='submit' value='Search' class='ucnesubmit'/><br/>

        </td>
      </tr>
    </tbody>
  </table>
  </form>
</div>
</p>

</div>


<!-- ######### Insert the footer #########-->
<?php readfile("footer.html"); ?>





</body>
</html>
