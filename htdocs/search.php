<?php readfile("header.php"); ?>
<?php require_once('config.inc.php'); ?>


<div style='font-size: 12px; text-align: justify;'>
<p>
<div id='advSearch'>
	<h2>Advanced search about <BR/><BR/>
		<div id='advSearch'>
         	<form>
    			<input type='radio' name='searchTerm' value='ucne' onclick='getSearchTerm(this.value)' checked='yes'/> UCNEs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    			<input type='radio' name='searchTerm' value='cluster' onclick="getSearchTerm(this.value)"/> UCNE clusters &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    			<input type='radio' name='searchTerm' value='gene' onclick='getSearchTerm(this.value)'/> Gene
    			<br/>
			</form>
		</div>
	</h2>
  <div id='advSearchOptions'>
  <table summary='Search the database' class='tblSearch' >
    <tbody>
      <th><td style='padding: 0px;'></td><td style='padding: 0px;'></td><td width=10% style='padding: 0px;'></td></th>
      <tr>
        <td class='type'>Overlapping with a chromosomal location in a species</td>
        <td class='links'>
        	<form action='list.php' method='get'>
        		<input type='hidden' name='data' value='ucne'>
        		<input type='hidden' name='view' value='region'>
				Specie: <select name='org'>
					<option value='hg19'>Human (hg19)</option>
					<option value='mouse'>Mouse (mm10)</option>
					<option value='armadillo'>Armadillo (dasNov1)</option>
					<option value='opossum'>Opossum (monDom5)</option>
					<option value='platypus'>Platypus (ornAna1)</option>
					<option value='gg3'>Chicken (galGal3)</option>
					<option value='zebra_finch'>Zebra_finch (taeGut1)</option>
					<option value='lizard'>Lizard (anoCar2)</option>
					<option value='painted_turtle'>Painted_turtle (chrPic1)</option>
					<option value='xenopus'>Xenopus (xenTro3)</option>
					<option value='fr2'>Fugu (fr2)</option>
					<option value='oryLat2'>Medaka (oryLat2)</option>
					<option value='gasAcu1'>Stickleback (gasAcu1)</option>
					<option value='tetNig2'>Tetraodon (tetNig2)</option>
					<option value='danRer7'>Zebrafish (danRer7)</option>
				</select><br/>
				Chromosome: <input type='text' value='chr1' onfocus="if(this.value==this.defaultValue) this.value='';" name='chr' size='10' maxlength='1024'/>
				Region from <input type='text' value='10000000' onfocus="if(this.value==this.defaultValue) this.value='';" name='start' size='10' maxlength='1024'/>
				to <input type='text' value='10800000' onfocus="if(this.value==this.defaultValue) this.value='';" name='stop' size='10' maxlength='1024'/>
        		<input type='submit' value='Search' class='ucnesubmit'/><br/>
			</form>
		</td>
      </tr>
      <tr>
      	<td class='type'>Located within a gene</td>
        <td class='links'>
        	<form action='list.php' method='get'>
        		<input type='hidden' name='data' value='ucne'>
        		<input type='hidden' name='org' value='hg19'>
        		<input type='hidden' name='view' value='gene'>
        		<input type='text' value='Type a gene name ...' onfocus="if(this.value==this.defaultValue) this.value='';" name='value' size='25' maxlength='1024'/>
        		<input type='submit' value='Search' class='ucnesubmit'/><br/>
        	</form>
        </td>
      </tr>
      <tr>
      	<td class='type'>In proximity of a gene</td>
        <td class='links'>
        	<form action='list.php' method='get'>
        		<input type='hidden' name='data' value='ucne'>
        		<input type='hidden' name='org' value='hg19'>
        		<input type='hidden' name='view' value='flank_gene'>
        		Flanking distance (bps) <input type='text' value='1000000' onfocus="if(this.value==this.defaultValue) this.value='';" name='flank' size='10' maxlength='1024'/>
        		of the gene: <input type='text' value='Type a gene name ...' onfocus="if(this.value==this.defaultValue) this.value='';" name='gene' size='20' maxlength='1024'/>
        		<input type='submit' value='Search' class='ucnesubmit'/><br/>
        	</form>
        </td>
      </tr>
      <tr>
      	<td class='type'>UCNEs longer than</td>
        <td class='links'>
        	<form action='list.php' method='get'>
        		<input type='hidden' name='data' value='ucne'>
        		<input type='hidden' name='org' value='hg19'>
        		<input type='hidden' name='view' value='bylength'>
        		length <input type='text' value='1000' onfocus="if(this.value==this.defaultValue) this.value='';" name='value' size='10' maxlength='1024'/> bps
        		<input type='submit' value='Search' class='ucnesubmit'/><br/>
        	</form>
        </td>
        <td width=10%></td>
     </tr>
    </tbody>
  </table>
  </div>
</div>
</p>

</div>


<!-- ######### Insert the footer #########-->
<?php readfile("footer.html"); ?>





</body>
</html>
