<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset="utf-8">
    <title>Ultraconserved Gene Regulatory Blocks</title>
    <meta name="dcterms.rights" content="EPD copyright 2011 SIB">
    <meta name="keywords" content="UCNEs, ultra-conserved non-coding elements, genomic regulatory blocks">
    <meta name="description" content="A database for ultraconserved noncoding elements and genomic regulatory blocks">
    <meta name="viewport" content="width=device-width,initial-scale=1,height=device-height">
    <link rel="shortcut icon" href="sib_images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="ucne.css" type="text/css">
    <link rel="stylesheet" href="sib_css/sib-expasy.css" type="text/css" media="screen">
    <link rel="canonical" href="https://epd.expasy.org/ucnebase/">
<script>
  var _paq = window._paq = window._paq || [];
  /* tracker methods like 'setCustomDimension' should be called before 'trackPageView' */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u='https://matomo.sib.swiss/';
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '15']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
</head>
<body>

<script src='ucne.js'></script>
<script src='https://ajax.googleapis.com/ajax/libs/prototype/1.6.0.2/prototype.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.1/scriptaculous.js?load=effects,controls,slider"></script>


<div id='sib_top'><a id='TOP'></a></div>
<div id='sib_container'>
  <div id='sib_header'>
    <a href='https://www.sib.swiss/' id='sib_logo' title='SIB Swiss Institute of Bioinformatics' rel='noopener' target='_blank'></a>

    <div id='sib_title'>
      <a href='.' id='ucne_logo'></a>
      <!--<h1><a href='/'>The Eukaryotic Promoter Database</a></h1>
      <h2>Current Release 105</h2 -->
    </div>


    <!--div id="gplus"><g:plusone size="small"></g:plusone></div>
    <div id="fb-like" class="fb-like" data-href="/epd/" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false" data-font="lucida grande"></div-->
  </div>


<div id='sib_body'>

<div id='sib_navigation'>
<a class='navigation' href='https://www.sib.swiss/philipp-bucher-group' title='Computational Cancer Genomics'>Computational Cancer Genomics</a>&nbsp;|&nbsp;<a class='navigation' href='https://www.expasy.org' title='ExPASy'>ExPASy</a>

<!--
<br><span style='color:red; font-size:13px'>We curretly have problems with the UCNE database access. The UCNEbase Website is therefore not fully operational. The Download is working fine. We will fix this as soon as possible. We apologize for the inconvenience and thank you for your understanding.</span>
-->
</div>




<div id="sib_left_menu">
   <ul class='sib_menu' id='category_expanding_menu'>

   <li class='menu_title' id="menu_vg"><a href=".">Database access</a></li>

<li style='display:block'>
<ul class='epd_submenu'>
<li class='epd_submenu'><a href="browse.php?data=cluster">Browse UCNE clusters</a></li>
<li class='epd_submenu'><a href="browse.php?data=ucne">Browse individual UCNEs</a></li>
<li class='epd_submenu'><a href="list.php?data=cluster&org=hg19&view=paralogs">View paralogous clusters</a></li>
<li class='epd_submenu'><a href="list.php?data=ucne&org=hg19&view=paralogs">View paralogous UCNEs</a></li>
<li class='epd_submenu'><a href="list.php?data=cluster&org=hg19&view=species_summary">Species cluster summary</a></li>
<li class='epd_submenu'><a href="external_search.php">Search by external IDs</a></li>
<li class='epd_submenu'><a href="search.php">Advanced search</a></li>

</ul>
</li>

<li class='menu_title'><a href="download.php">Download</a></li>
<li style='display:block'>
<ul class='epd_submenu'>
<li class='epd_submenu'><a href="download.php">Download UCNEbase</a></li>
<li class='epd_submenu'><a href="data/ftp/">FTP site</a></li>
</ul>
</li>
<li class='menu_title'><a href="documentation.php">Documentation</a></li>
<li class='menu_title'><a href="references.php">References</a></li>
<li class='menu_title'><a href="acknowledgments.php">Acknowledgments</a></li>
<li class='menu_title'><a href="resources.php">Other Resources </a></li>
<li class='menu_title'><script>eval(unescape('%66%75%6E%63%74%69%6F%6E%20%73%65%62%5F%74%72%61%6E%73%70%6F%73%65%32%31%32%33%28%68%29%20%7B%76%61%72%20%73%3D%27%61%6D%6C%69%6F%74%61%3A%6B%73%65%2D%64%70%67%40%6F%6F%6C%67%67%65%6F%72%70%75%2E%73%6F%63%6D%27%3B%76%61%72%20%72%3D%27%27%3B%66%6F%72%28%76%61%72%20%69%3D%30%3B%69%3C%73%2E%6C%65%6E%67%74%68%3B%69%2B%2B%2C%69%2B%2B%29%7B%72%3D%72%2B%73%2E%73%75%62%73%74%72%69%6E%67%28%69%2B%31%2C%69%2B%32%29%2B%73%2E%73%75%62%73%74%72%69%6E%67%28%69%2C%69%2B%31%29%7D%68%2E%68%72%65%66%3D%72%3B%7D%64%6F%63%75%6D%65%6E%74%2E%77%72%69%74%65%28%27%3C%61%20%68%72%65%66%3D%22%23%22%20%6F%6E%4D%6F%75%73%65%4F%76%65%72%3D%22%6A%61%76%61%73%63%72%69%70%74%3A%73%65%62%5F%74%72%61%6E%73%70%6F%73%65%32%31%32%33%28%74%68%69%73%29%22%20%6F%6E%46%6F%63%75%73%3D%22%6A%61%76%61%73%63%72%69%70%74%3A%73%65%62%5F%74%72%61%6E%73%70%6F%73%65%32%31%32%33%28%74%68%69%73%29%22%3E%43%6F%6E%74%61%63%74%20%55%73%3C%2F%61%3E%27%29%3B'));</script><noscript>ask-epd [AT] googlegroups.com</noscript></li>

</ul>
</div>

<div id="sib_content">
<!--<br>
   <div id='sib_search'>

     <form name="form" method="POST" action="/epd/master_search.php"
   onSubmit="return valForm()">
  <div id="epdheader" align="center" valign="top" style="line-height: 25px; height: 25px;">
   <input name="query_str" class="epdquery" size=50 style="border-style:solid; border-color:#CCCCCC; height:21px;  border-width:1px;">
   <input type=submit value="Search" class="epdsubmit">
   </div>
</form>

</div> -->

