```bash
export UCNEBASE_VERSION=1.0.1
```

# Run the container with default -> MySQL and Apache running
```bash
docker run -p 8081:8081 -d ucnebase:$UCNEBASE_VERSION
```
- -p  Publish a container's port(s) to the host, i.e. map internal port 8081 to 8081 externally
- -d  Run container in background and print container ID

The local container instance is accessible at **http://localhost:8081/**

### Monitoring the running container
```bash
docker ps
```

### Killing/stopping the running container
```bash
docker kill <CONTAINER ID>
```


# Run bash in the Docker image
```bash
docker run --rm -i -t ucnebase:$UCNEBASE_VERSION bash
```
### Mounting/binding a local repository (,readonly can be added to force readonly mounting)
```bash
    --mount type=bind,source=/software,target=/software
```
- UCNEBASE_VERSION is the container version
- --name assignes a name to the running container
- --rm automatically removes the container when it exits
- -i opens an interactive session with the container
- -t allocates a pseudo-TTY
