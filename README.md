TODO
- /data/download/clusters/galGal3_subclusters.txt is missing
- Set local all external JS/CSS calls


DONE
- Create a docker hub account for EPD? => No, use the SIB (sibswiss) one
- How to push several times the same container but for different architectures on docker hub? => See docker/README files
- Send e-mails from the container? At least port 25 should be exposed => No more email form, we use ask-epd@googlegroups.com
- HTTPS certificate can be done on the host machine, with a proxy to/from the container
- Apache without SSL/HTTPS and mod_security in the container
- Apache different ports for each base, e.g. 8081 for UCNEbase, 8082 for EPD
- Links to UCSC genome browser look preserved
- License? Use GPLv3
- Use CentOS 7 or a more long term support (LTS) OS? Ubuntu is smaller and LTS lasts longer
- MySQL conf in a container? With Ubuntu, the root user can access MySQL as root, whereas with CentOS MySQL root password is lost in vanishing log in the container!
- Replaced the FTP link by a local directory (because of issues with the FTP protocol)
- Found missing /ucne/UCNE_subclusters_*.xls file on the FTP partition
- Add current MySQL dump download
- Some .htaccess files are there! Maybe move the content to the main apache conf, or move the folder elsewhere!
- Keep contact e-mails ask-epd [AT] googlegroups.com
- A lot of .bkp files are also there! And some *_old*, *.tmp* files also!
- What to do with all duplicated files and folders e.g. ucnes/ content; which sib-expasy.css file is really used ???
- The htdocs/icons/ has been renamed because by default apache export a /icons that overrides ours!
- Correct publication link
